import React from "react";
import TabCartForm from "./TabCartForm";
import "./Css2.css"
import "./Css1.css"
import "./Desktop.css"
import "./Standard.css"

function PreBooking() {
  return (
    <div id="desktopContentV3" class="desktopV3">
      <div>
        <div class="_2XbV5"></div>
        <div>
          <div>
            <div>
              <div class="kMmuG"></div>
              <div class="_2XbV5">
                <div class="RAcQT " >
                  <div>
                    <div class="css-1dbjc4n ">
                      <div
                        class="css-1dbjc4n r-kdyh1x  titleprebooking r-1yos0t3 r-1udh08x r-nsbfu8"
                        style={{background:"white"}
                      }
                      >
                        <div class="css-1dbjc4n r-18u37iz">
                          <h2
                            aria-level="2"
                            dir="auto"
                            role="heading"
                            class="css-4rbku5 css-901oao r-1sixt3s r-adyw6z r-b88u0q r-135wba7 r-fdjqy7"
                            style={{color: "black"}}
                          >
                            Flight from Hà Nội to TP HCM
                          </h2>
                          <div class="css-1dbjc4n r-13awgt0 r-18u37iz r-17s6mgv">
                            <span class="_2it9m">
                              <div class="css-1dbjc4n">
                                <svg
                                  stroke-width="0"
                                  fill="#0770CD"
                                  height="24"
                                  stroke="currentColor"
                                  stroke-linecap="round"
                                  viewBox="0 0 24 24"
                                  width="24"
                                >
                                  <g fill="none" fill-rule="evenodd">
                                   
                                  </g>
                                </svg>
                              </div>
                            </span>
                          </div>
                        </div>
                        <div class="css-1dbjc4n r-18u37iz r-1wzrnnt">
                          <div class="css-1dbjc4n r-1kb76zh r-1bymd8e">
                            <svg
                              stroke-width="0"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="#30C5F7"
                              stroke="currentColor"
                              stroke-linecap="round"
                            ></svg>
                          </div>
                          <div class="css-1dbjc4n">
                            <div
                              dir="auto"
                              class="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                              style={{color:" rgb(104, 113, 118);"}}
                            >
                              Hà Nội (HAN) → TP HCM (SGN)
                              &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; 8 Th06 2021
                            </div>
                            <div
                              dir="auto"
                              class="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                              style={{color:" rgb(104, 113, 118);"}}
                            >
                              1 người lớn &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                              Economy
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix WHSLl">
                  <div class="_2H3oJ">
                    <div>
                      <div class="RAcQT">
                        <div>
                          <div>
                            <div class="_3Ec21">
                              <div class="mMmI2 _2BAJV" data-elevation="1">
                                <div class="_31_PF">
                                  <span class="_22IcR">
                                    <svg
                                      stroke-width="0"
                                      fill="#30C5F7"
                                      height="24"
                                      stroke="currentColor"
                                      stroke-linecap="round"
                                      viewBox="0 0 24 24"
                                      width="24"
                                    >
                                      <g>
                                      </g>
                                    </svg>
                                  </span>
                                  <h4 class="_2rus_ PiNWC">
                                    <div>
                                      Hà Nội (HAN)
                                      <svg
                                        viewBox="0 0 25 25"
                                        fill="#8f8f8f"
                                        height="24"
                                        stroke="currentColor"
                                        stroke-linecap="round"
                                        width="24"
                                      >
                                        <g transform="translate(1.000000, 1.000000)">
                                          <g
                                            stroke="none"
                                            stroke-width="1"
                                            fill="none"
                                            fill-rule="evenodd"
                                          >
                                            <g fill="#8f8f8f">
                                              <g>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </svg>
                                      TP HCM (SGN)
                                    </div>
                                  </h4>
                                  <a class="EA3rH">Thông tin chuyến bay</a>
                                </div>
                                <div class="_3HMVE">
                                  <div class="_1BH3r">
                                    <div class="css-1dbjc4n">
                                      <div class="css-1dbjc4n"></div>
                                      <h3
                                        aria-level="3"
                                        dir="auto"
                                        role="heading"
                                        class="css-4rbku5 css-901oao r-1sixt3s r-ubezar r-b88u0q r-rjixqe r-fdjqy7"
                                        style={{color: "rgb(3, 18, 26);"}}
                                      >
                                        Tue, 08 Jun 2021
                                      </h3>
                                      <div class="_10ZkQ _1CMmO">
                                        <div
                                          class="_1ZBvW"
                                        ></div>
                                        <div>
                                          <div>
                                            <span class="_1KrnW jjGhl _2HSse">
                                              VietJet Air
                                            </span>
                                          </div>
                                          <span class="_1KrnW _1EnnQ Edww1">
                                            Khuyến mãi
                                          </span>
                                        </div>
                                        <div>
                                          <span class="_1KrnW _1EnnQ Edww1 _3AtUV"></span>
                                        </div>
                                      </div>
                                      <div>
                                        <div class="_10ZkQ">
                                          <div class="OMkfM">
                                            <div>
                                              <div>
                                                <span class="_1KrnW jjGhl _2HSse">
                                                  12:20
                                                </span>
                                              </div>
                                              <span class="_1KrnW _1EnnQ Edww1">
                                                Hà Nội (HAN)
                                              </span>
                                            </div>
                                            <div class="_1m9Ud">
                                              <svg
                                                stroke-width="0"
                                                width="12"
                                                height="12"
                                                viewBox="0 0 12 12"
                                                fill="#8f8f8f"
                                                stroke="currentColor"
                                                stroke-linecap="round"
                                              >
                                                
                                              </svg>
                                            </div>
                                            <div>
                                              <div>
                                                <span class="_1KrnW jjGhl _2HSse">
                                                  14:30
                                                </span>
                                              </div>
                                              <span class="_1KrnW _1EnnQ Edww1">
                                                TP HCM (SGN)
                                              </span>
                                            </div>
                                            <div>
                                              <div>
                                                <span class="_1KrnW jjGhl _2HSse">
                                                  2h 10m
                                                </span>
                                              </div>
                                              <span class="_1KrnW _1EnnQ Edww1">
                                                <span class="_3Hkm2 _8Yf_q"></span>
                                                Bay thẳng
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    class="css-1dbjc4n"  
                                    
                                  >
                                    <div
                                      class="css-1dbjc4n r-nsbfu8"
                                      style={{alignitems:" stretch;"},
                                       {flexdirection: "column;"},
                                    {background: "whitesmoke"}, {borderradius: "4px;"},  {marginbottom: "0 -16px -16px -16px;"}
                                      }
                                    >
                                      <div
                                        dir="auto"
                                        class="css-901oao r-1sixt3s r-ubezar r-b88u0q r-135wba7 r-fdjqy7"
                                        id="fl-upsell-title"
                                        style={{color: "rgb(3, 18, 26);"}}
                                      >
                                        Nâng tầm trải nghiệm bay của bạn
                                      </div>
                                      
                                      <TabCartForm />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="_3EMRU">
                    <div>
                      <div>
                        <div class="mMmI2 _2BAJV" data-elevation="1">
                          <div class="_31_PF">
                            <span class="_22IcR">
                              <svg
                                fill="#CCCCCC"
                                height="24"
                                stroke="currentColor"
                                stroke-linecap="round"
                                viewBox="0 0 24 24"
                                width="24"
                              >
                                <g fill="none" fill-rule="evenodd">
                                 
                                </g>
                              </svg>
                            </span>
                            <h4 class="_2rus_ PiNWC">Thông tin bổ sung</h4>
                            <span class="_1KrnW jjGhl box20 _3Thek EA3rH">
                              Chi tiết
                            </span>
                          </div>
                          <div class="_3HMVE">
                            <div>
                              <div class="_2wQwb">
                                <span class="_1KrnW _1EnnQ _2i_vU _1dKIX _2JlHX _3Tjml">
                                  HAN →SGN
                                </span>
                                <div>
                                  <span class="_1KrnW _1EnnQ Edww1 _22csp -TgC7">
                                    <svg
                                      stroke-width="0"
                                      width="16"
                                      height="16"
                                      viewBox="0 0 16 16"
                                      fill="#8F8F8F"
                                      class="_3GhTG"
                                      stroke="currentColor"
                                      stroke-linecap="round"
                                    >
                                      <g
                                        fill="none"
                                        fill-rule="evenodd"
                                        stroke-width="0"
                                      >
                                        <rect width="16" height="16"></rect>
                                        <circle
                                          cx="8"
                                          cy="8"
                                          r="7"
                                          stroke="#8F8F8F"
                                          stroke-width="2"
                                        ></circle>
                                      
                                        <circle
                                          cx="8"
                                          cy="5"
                                          r="1"
                                          fill="#8F8F8F"
                                        ></circle>
                                      </g>
                                    </svg>
                                    <span>Không hoàn tiền</span>
                                  </span>
                                  <span class="_1KrnW _1EnnQ KVDS- _22csp -TgC7">
                                    <svg
                                      stroke-width="0"
                                      width="16"
                                      height="16"
                                      viewBox="0 0 16 16"
                                      fill="#00A651"
                                      class="_3GhTG"
                                      stroke="currentColor"
                                      stroke-linecap="round"
                                    >
                                      <g fill="none" fill-rule="evenodd">
                                       
                                        <circle
                                          stroke="#00A651"
                                          stroke-width="2"
                                          cx="8"
                                          cy="8"
                                          r="7"
                                        ></circle>
                                      
                                      </g>
                                    </svg>
                                    <span>Có áp dụng đổi lịch bay</span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="mMmI2 _1k6Wf" data-elevation="1">
                        <div class="_37BLH">
                          <div class="_1ibeJ">
                            <h4 class="_2rus_">Tóm tắt</h4>
                          </div>
                          <div class="_23HcW"></div>
                        </div>
                        <hr class="_2si0n" data-inset="false"/>
                          <div class="_2U03q _3s0B1">
                            <table class="_3i62q">
                              <tbody>
                                <tr class="_228at">
                                  <td class="_22Q3K">
                                    <span class="_1KrnW _1EnnQ _2HSse">
                                      VietJet Air (Người lớn) x1
                                    </span>
                                  </td>
                                  <td class="_22Q3K">
                                    <div class="_2Zal8">
                                      <span class="_1KrnW _1EnnQ _2HSse _2LGIU">
                                        VND 554.900
                                      </span>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="_2G3EG" colspan="3">
                                    <div class="_2U03q _1th4p">
                                      <div class="KfmJk">
                                        <div class="_3aWpR _1so4K">
                                          <span class="_1KrnW jjGhl _2HSse">
                                            Giá bạn trả
                                          </span>
                                          <div
                                            data-id="priceSummaryTotal"
                                            class="_3CN4A"
                                          >
                                            <span class="_1KrnW jjGhl _2HSse">
                                              VND 554.900
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        <button
                          class="_1MyvW _9Srcv gLbQ- _90_75"
                          type="button"
                          data-id="tripConfirmStdzPrebooking"
                        >
                          Tiếp tục
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    // // <script nonce="" type="text/javascript">
    // //     (function() {
    // //       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    // //       ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    // //       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    // //     })();</script>
    // //     <script nonce="">
    // //     var marker = document.getElementsByTagName('body')[0];
    // //     marker.setAttribute('data-has-js-error', false);

    // //     window.onerror = function(error) {
    // //       var marker = document.getElementsByTagName('body')[0];
    // //       marker.setAttribute('data-has-js-error', true);
    // //       marker.setAttribute('data-js-error', error.toString());
    // //     }</script>
    //     <script nonce="" type="text/javascript">
    //         window.featureControl = {"test":{"enabled":false,"properties":{}},"tx-list-payment-filter-kiosk":{"enabled":"true","properties":{"paymentMethodName":"Kiosk","group":["MOLPAY_COUNTER"]}},"mweb-airport-transport-landing-page-merchandising":{"enabled":"true"},"tx-list-payment-filter-counter-payment":{"enabled":"true","properties":{"paymentMethodName":"Tại điểm thu tiền","group":["ONETWOTHREE_COUNTER"]}},"xperience-search-modal":{"enabled":"true"},"flight-desktop-airline-page-search-box":{"enabled":"true","properties":{"group":["flight-hotel"]}},"rt-headless":{"enabled":"true","properties":{"officeIp":"true"}},"payment-direct-upload":{"enabled":"true"},"tx-list-payment-filter-paylater":{"enabled":"true","properties":{"paymentMethodName":"PayLater","group":["CREDIT_LOAN"]}},"tx-list-payment-filter-cimb-click":{"enabled":"true","properties":{"paymentMethodName":"CIMB Clicks","group":["CIMB_CLICKS"]}},"gift-voucher-search":{"enabled":"true"},"mweb-airport-transport-onboarding":{"enabled":"false"},"mybooking-merchandising-top":{"enabled":"false","properties":{"title":"My Booking Merchandising","storefront":"myBooking","pageName":"myBooking-Active-Bottom-LandingPage"}},"vehicle-rental-search":{"enabled":"true"},"COLLECTION_FLIGHT":{"enabled":"true"},"tx-list-payment-filter-credit-card":{"enabled":"true","properties":{"paymentMethodName":"Thẻ tín dụng","group":["CREDIT_CARD"]}},"mweb-rail-menu":{"enabled":"true"},"tx-list-payment-filter-bank-deposit":{"enabled":"true","properties":{"paymentMethodName":"Dragonpay","group":["DRAGON_PAY"]}},"payment-error-handler":{"enabled":"true"},"top-menu-app-only-group":{"enabled":"false","properties":{"transports":["flight-status","price-alert"],"accomodations":["hotel-voucher"],"travel-addons":["points-search","gift-voucher-search"]}},"top-menu-group":{"enabled":"true","properties":{"transports":["flight-search","tomang-search","airport-transport","vehicle-rental-search","train-global-search"],"things-to-do":["activity-search"],"accomodations":["hotel-search","tomang-search"]}},"tx-list-payment-filter-ibanking":{"enabled":"true","properties":{"paymentMethodName":"Internet Banking","group":["MOLPAY_EBANKING"]}},"hotel-seo-country":{"enabled":"true"},"LANDMARK_CLICKABLE":{"enabled":"false"},"saved-item-clickable-article":{"enabled":"true"},"vehicle-rental-onboarding":{"enabled":"true"},"VILLA_AND_APARTMENT_CLICKABLE":{"enabled":"false"},"saved-item-template-list":{"enabled":false,"properties":{"group":["FLIGHT_ENABLED","HOTEL_ENABLED","VILLA_AND_APARTMENT_ENABLED","EXPERIENCE_ENABLED","CULINARY_ENABLED","CULINARY_DEAL_ENABLED","DESTINATION_ENABLED","LANDMARK_ENABLED","ARTICLE_ENABLED"]}},"EXPERIENCE_ENABLED":{"enabled":"true"},"show-announcement":{"enabled":"true","properties":{"cardImage":"https://ik.imagekit.io/tvlk/image/imageResource/2020/12/08/1607410621598-e3654d0eaf095fd6a13d84a7017cda09.jpeg","cta":"<a href=\"https://www.traveloka.com/vi-vn/vietnam-coronavirus-information\">See full updates.<\/a>","link":"https://www.traveloka.com/vi-vn/flight/safe-travel","message":"All you need to know about status and policy of flights during Corona Virus Outbreak."}},"tx-list-payment-filter-bca-klikpay":{"enabled":"true","properties":{"paymentMethodName":"BCA Klikpay","group":["BCA_KLIKPAY"]}},"search-box-app-only":{"enabled":"false","properties":{"group":["hotel-voucher","points-search","gift-voucher-search","flight-status","price-alert"]}},"referral.block.sound":{"enabled":false,"properties":{"blockWebViewSource":"none"}},"embedded-PAH-form":{"enabled":"true"},"flight-reschedule-revamp":{"enabled":"true"},"saved-item":{"enabled":"true","properties":{"group":["saved-flight","saved-hotel","saved-activity","saved-eats"]}},"transaction-list-payment-filter-php":{"enabled":"true","properties":{"group":["tx-list-payment-filter-credit-card","tx-list-payment-filter-bank-transfer","tx-list-payment-filter-over-the-counter","tx-list-payment-filter-paypal","tx-list-payment-filter-bank-deposit"]}},"enable-payment-confirmation-mobile":{"enabled":"true","properties":{"officeIp":"false","url":"/payment/confirmation"}},"mweb-airport-transport-on-demand-search-form":{"enabled":"true"},"remove-coupon":{"enabled":"true"},"HOTEL_ENABLED":{"enabled":"true"},"mweb-rail-enabled":{"enabled":"true"},"hotel-detail-consistency":{"enabled":"true"},"hotel-new-booking-review":{"enabled":"true"},"afterLoginPopup-booking":{"enabled":"true"},"hotel-promo":{"enabled":"true"},"multiple-points":{"enabled":"true"},"new-flight-autocomplete":{"enabled":"true","properties":{"officeIp":"false"}},"transaction-list-payment-filter-usd":{"enabled":"true","properties":{"group":["tx-list-payment-filter-credit-card","tx-list-payment-filter-paypal"]}},"hcn-search-mobile":{"enabled":"true"},"activity-mobile-seat":{"enabled":"true"},"hotel-seo-new":{"enabled":"true"},"saved-item-clickable-experience":{"enabled":"true"},"hcn-search":{"enabled":"true"},"tx-list-payment-filter-direct-debit":{"enabled":"true","properties":{"paymentMethodName":"Direct Debit","group":["DIRECT_DEBIT"]}},"app-download-banner-mweb":{"enabled":"true"},"flight-corporate":{"enabled":false,"properties":{"flight-search-form":"false","mweb-corporate":"false","flight-search-result":"false"}},"flight-round-trip":{"enabled":"true","properties":{"officeIp":"true"}},"flight-route-new-seo":{"enabled":"true"},"saved-item-template-clickable":{"enabled":false,"properties":{"group":["FLIGHT_CLICKABLE","HOTEL_CLICKABLE","VILLA_AND_APARTMENT_CLICKABLE","EXPERIENCE_CLICKABLE","CULINARY_CLICKABLE","CULINARY_DEAL_CLICKABLE","DESTINATION_CLICKABLE","LANDMARK_CLICKABLE","ARTICLE_CLICKABLE"]}},"enable-payment-mycards":{"enabled":"true","properties":{"url":"/travelokaPay/cards"}},"serviceworker":{"enabled":"true"},"description-object":{"enabled":"true"},"airport-transport-product":{"enabled":"true"},"flight-status":{"enabled":"true"},"flight-desktop-discover":{"enabled":"true","properties":{"dealsSectionNames":["entry point 1","banner"]}},"saved-hotel":{"enabled":"true"},"activity-booking-form-old":{"enabled":"false"},"reactions":{"enabled":"true","properties":{"products":["HOTEL","CULINARY","VEHICLE_RENTAL","connectivityRoaming","connectivityPrepaidSim","connectivityWifiRental","EXPERIENCE"]}},"activity-desktop-saved-item":{"enabled":"true"},"hotel-desktop-detail-saved-item-button":{"enabled":"true"},"bpjs-search":{"enabled":"true"},"booking-experimental":{"enabled":"false","properties":{"mode":"public"}},"tx-list-payment-filter-uangku":{"enabled":"true","properties":{"paymentMethodName":"UANGKU Balance","group":["WALLET_CASH"]}},"flight-one-way":{"enabled":"true","properties":{"officeIp":"true"}},"tx-list-payment-filter-indomaret":{"enabled":"true","properties":{"paymentMethodName":"Indomaret","group":["PAYMENT_POINT"]}},"alternative-apartment":{"enabled":"true"},"flight-promo":{"enabled":"true"},"hotel-mweb-new-tab":{"enabled":"true"},"hotel-pah-promo-homepage":{"enabled":"true"},"mobile-bottom-navigation":{"enabled":"true","properties":{"group":["nav-home","nav-my-booking","nav-my-account","nav-saved-list"]}},"transaction-list-payment-filter-eur":{"enabled":"true","properties":{"group":["tx-list-payment-filter-credit-card","tx-list-payment-filter-paypal"]}},"VILLA_AND_APARTMENT_ENABLED":{"enabled":"true"},"saved-flight":{"enabled":"true"},"hotel-theme-picker":{"enabled":"true"},"hotel-family-checkbox":{"enabled":"true"},"hotel-extrabed":{"enabled":"true"},"saved-item-clickable-flight":{"enabled":"true"},"vehicle-rental-tab-types":{"enabled":false,"properties":{"types":"[\"WITH_DRIVER\", \"WITHOUT_DRIVER\"]"}},"fl-public-holiday":{"enabled":"true","properties":{"origin":"39","destination":"10"}},"flight-desktop-destination-page-search-box":{"enabled":"true","properties":{"group":["flight-hotel","airport-transport"]}},"activity-search":{"enabled":"true"},"hotel-default-occupancy":{"enabled":"true","properties":{"value":"2"}},"vehicle-rental-postbooking-boarding-info":{"enabled":"true"},"hotel-pah-confirmation-method-rule":{"enabled":"true"},"ENABLE_ALTERNATIVE_ACCOM_ENTRY":{"enabled":"true"},"EXPERIENCE_CLICKABLE":{"enabled":"true"},"activity-detail-app-api":{"enabled":"true"},"app-push-modal-mweb":{"enabled":"true"},"mweb-airport-transport":{"enabled":"true"},"hotel-child-occupancy":{"enabled":"true"},"activity-product-pd-mod":{"enabled":"true"},"nu-data":{"enabled":false,"properties":{"mobileLogin":["1"],"desktopLogin":["1"],"desktopSelectPayment":["1"],"mobileSelectPayment":["1"],"desktopLoginWidget":["1"]}},"enable-mobile-payment-confirmation":{"enabled":"false"},"all-button":{"enabled":"true"},"ENABLE_ALTERNATIVE_ACCOM_ENTRY_MWEB":{"enabled":"true"},"desktop-home-push-notif":{"enabled":"true"},"fl-pronto":{"enabled":"true"},"COLLECTION_EXPERIENCE":{"enabled":"true"},"tx-list-payment-filter-over-the-counter":{"enabled":"true","properties":{"paymentMethodName":"Over-the-Counter","group":["COINS","PAYNAMICS","SEVEN_ELEVEN"]}},"hotel-search":{"enabled":"true"},"hotel-voucher":{"enabled":"true"},"flight-spacer-top":{"enabled":"true","properties":{"height":"55px"}},"flight-multi-city":{"enabled":"true","properties":{"officeIp":"true","isSavedItemsEnabled":"false","isCorporateEnabled":"false","corporateBlocked":"PERTAMINA_HQ,ASTRAOTOPARTS_B2B_MANUAL,JALURMANDIRI_B2B_MANUAL","isMonitorPriceEnabled":"false","maxAirport":"5","minAirport":"2","isRetailCorporateEnabled":"false"}},"hotel-bpg-upload":{"enabled":"true"},"tx-list-payment-filter-mandiri-clickpay":{"enabled":"true","properties":{"paymentMethodName":"Mandiri Clickpay","group":["MANDIRI_CLICKPAY","MANDIRI_CLICKPAY_WEB"]}},"hotel-new-booking-form":{"enabled":"true"},"saved-item-template-filter-label":{"enabled":false,"properties":{"CULINARY":"Ẩm thực","LANDMARK":"Điểm mốc du lịch","ARTICLE":"Bài viết","FLIGHT":"Vé máy bay","VILLA_AND_APARTMENT":"Villas & Apartments","DESTINATION":"Điểm đến du lịch","EXPERIENCE":"Xperience","CULINARY_DEAL":"Eats - Special Offers","HOTEL":"Khách sạn"}},"LANDMARK_ENABLED":{"enabled":"true"}}
    //                 </script>
    //             <script type="text/javascript" nonce="">var _cf = _cf || [];  _cf.push(['_setFsp', true]);  _cf.push(['_setBm', true]);  _cf.push(['_setAu', '/clientlibs/0e73dfc15no21135e54f2916f72af6d']); </script>
    //             <script type="text/javascript" nonce="" src="js/dc.js" gapi_processed="true"></script>
    //             <script nonce="" src="js/generalRoute-release-17965-45b4ec5f3f.js"></script>
    //             <script nonce="" src="js/desktop-externals-5910643252a960e0db91.js" type="text/javascript"></script>
    //             <script nonce="" src="js/standardization-prebooking-form-b578ca8750a339fd6031.js" type="text/javascript"></script>
    // <link rel="stylesheet" type="text/css" href="css/common-desktop-v3-6765aad05a6a6e2d7c85.css">
    // <link rel="stylesheet" type="text/css" href="css/standardization-prebooking-form-eaac56a711afc72618cf.css">
    // <link rel="stylesheet" type="text/css" href="css/75-d35dafd1bd6aaf5a174d.css">
    // <link rel="stylesheet" type="text/css" href="css/81-64724d9d0c2330295351.css">
  );
}
export default PreBooking;
