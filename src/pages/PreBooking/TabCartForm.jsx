import React, { Component } from "react";
import Tabcart1 from "./Tabcart1";

import TabSelectCart from "./TabSelectCart";
import Panel from "./Panel";

class TabCartForm extends Component {
  render() {
    return (
      <div >
        <TabSelectCart className="tab-cart " style={{backgroundcolor: "rgb(242, 243, 243);"},{display:"flex"} } /*tab cha*/>
          <Panel title="Eco" className="Panel" /*tabs con*/>
            <div className="content-cart" /*Hiển thị nội dung tabs con */>
              <Tabcart1 />
            </div>
          </Panel>
          <Panel title="Deluxe" className="Panel">
            <div className="content-cart">
            </div>
          </Panel>
         
        </TabSelectCart>
      </div>
    );
  }
}

export default TabCartForm;
