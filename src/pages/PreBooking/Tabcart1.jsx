import React, { useState } from "react";

import { IoIosAirplane } from "react-icons/io";
function Tabcart1() {
  return (
    <div>
      <div className="tab-cart1" style={{ color: "rgb(3, 18, 26);" }}>
        <h2> Bạn sẽ nhận được gì</h2>
        <div>
          <span>
            <img
              src="https://ik.imagekit.io/tvlk/image/imageResource/2020/10/22/1603364972008-49316019046ab972566c44df11d0b9ac.png?tr=h-16,q-75,w-16"
              importance="low"
              loading="lazy"
              srcset="https://ik.imagekit.io/tvlk/image/imageResource/2020/10/22/1603364972008-49316019046ab972566c44df11d0b9ac.png?tr=h-16,q-75,w-16 1x, https://ik.imagekit.io/tvlk/image/imageResource/2020/10/22/1603364972008-49316019046ab972566c44df11d0b9ac.png?tr=dpr-2,h-16,q-75,w-16 2x, https://ik.imagekit.io/tvlk/image/imageResource/2020/10/22/1603364972008-49316019046ab972566c44df11d0b9ac.png?tr=dpr-3,h-16,q-75,w-16 3x"
              width="16"
              height="16"
              style={({ objectfit: "fill" }, { objectposition: "50% 50%;" })}
            />
          </span>
          <span> 1 x 7KG Hành lý xách tay.</span>
        </div>
        <div>
          <span>
            {" "}
            <img
              src="https://ik.imagekit.io/tvlk/image/imageResource/2020/11/02/1604298564639-69679b53c61b1c97c45400c1207533e0.png?tr=h-16,q-75,w-16"
              importance="low"
              loading="lazy"
              srcset="https://ik.imagekit.io/tvlk/image/imageResource/2020/11/02/1604298564639-69679b53c61b1c97c45400c1207533e0.png?tr=h-16,q-75,w-16 1x, https://ik.imagekit.io/tvlk/image/imageResource/2020/11/02/1604298564639-69679b53c61b1c97c45400c1207533e0.png?tr=dpr-2,h-16,q-75,w-16 2x, https://ik.imagekit.io/tvlk/image/imageResource/2020/11/02/1604298564639-69679b53c61b1c97c45400c1207533e0.png?tr=dpr-3,h-16,q-75,w-16 3x"
              width="16"
              height="16"
              style={({ objectfit: "fill;" }, { objectposition: "50% 50%;" })}
            />
          </span>
          <span>
            {" "}
            Có thể đổi lịch (có thể áp dụng phí đổi lịch và chênh lệch giá vé)
          </span>
        </div>
      </div>
    </div>
  );
}
export default Tabcart1;
