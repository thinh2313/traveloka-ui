import React from 'react';

export const Confirm = () => {
  return (
    <div id="desktopContentV3" className="desktopV3">
      <div className="fRTmT">
        <div />
        <div className="_30ZP9">
          <h3 className="_3_nMb">
            <span className="_2m3J1">
              <a href="#">
                <svg
                  strokeWidth={0}
                  fill="#0770cd"
                  height={24}
                  stroke="currentColor"
                  strokeLinecap="round"
                  viewBox="0 0 24 24"
                  width={24}
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                >
                  <g>
                    <path d="M5.41421356,13 L11.7071068,19.2928932 C12.0976311,19.6834175 12.0976311,20.3165825 11.7071068,20.7071068 C11.3165825,21.0976311 10.6834175,21.0976311 10.2928932,20.7071068 L2.29289322,12.7071068 C1.90236893,12.3165825 1.90236893,11.6834175 2.29289322,11.2928932 L10.2928932,3.29289322 C10.6834175,2.90236893 11.3165825,2.90236893 11.7071068,3.29289322 C12.0976311,3.68341751 12.0976311,4.31658249 11.7071068,4.70710678 L5.4075491,11 L21.0052288,11 C21.5546258,11 22,11.4438648 22,12 C22,12.5522847 21.5490746,13 21.0052288,13 L5.41421356,13 L5.41421356,13 Z" />
                  </g>
                </svg>
              </a>
            </span>
            Transfer Payment Instructions
          </h3>
        </div>
        <div className="_9zZkO">
          <div className="_30ZP9">
            <div className="_3Uc1x">
              <div>
                <div className="njSVf">Hướng dẫn thanh toán đã được gửi vào email của bạn.</div>
                <div className="_3KXxS ">
                  <div className="_2gdIP">
                    <div className="_3bY-J">1</div>
                    <div className="_1grPh">Tiến hành thanh toán trước</div>
                  </div>
                  <div className="_3IWfa">
                    <div>
                      <div className="lBo19">
                        <h2>Hôm nay 18:48 PM</h2>
                        <p>Tiến hành thanh toán trong vòng 24 phút 4 giây</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="_1Xbi-">
                  <div className="_3KXxS ">
                    <div className="_2gdIP">
                      <div className="_3bY-J">2</div>
                      <div className="_1grPh">Vui lòng chuyển khoản đến:</div>
                    </div>
                    <div className="_3IWfa">
                      <div>
                        <div className="_17-KQ">
                          <div className="_2kdh9">
                            <div className="_2FchJ">
                              <h2>Vietcombank</h2>
                              <div className="DweQM">
                                <img
                                  className="PPoKG"
                                  src="https://ik.imagekit.io/tvlk/image/imageResource/2017/03/20/1489981886316-4f45965c5fb8a6d379a327f0f7cfcd42.png?tr=q-75"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="_3nGK6">
                            <div className="_3VI7o">
                              <div className="_3LSFN">Số tài khoản:</div>
                              <div className="_35p4I">
                                <p>
                                  <b>0071001088218</b>
                                </p>
                                <p />
                              </div>
                            </div>
                            <div className="_3VI7o">
                              <div className="_3LSFN">Chủ tài khoản:</div>
                              <div className="_35p4I">
                                <p>
                                  <b>CTY TNHH TRAVELOKA VIỆT NAM</b>
                                </p>
                                <p />
                              </div>
                            </div>
                            <div className="_3VI7o">
                              <div className="_3LSFN">Nội dung thanh toán:</div>
                              <div className="_35p4I">
                                <p>
                                  <b>Thanh toán đặt chỗ 701557386</b>
                                </p>
                                <p />
                              </div>
                            </div>
                          </div>
                          <div>
                            <div className="_23bCk">
                              <div className="_3VI7o">
                                <div className="_3LSFN">Số tiền chuyển khoản:</div>
                                <div className="_35p4I">
                                  <p>
                                    <b>
                                      554.<span className="_1OrUs">986</span>
                                    </b>
                                  </p>
                                  <div />
                                  <div className="_2V1EY" />
                                  <b> VND</b>
                                  <p />
                                </div>
                              </div>
                            </div>
                            <div className="_1OrUs _2KyYQ">
                              <span className="ugCaG">
                                <span>
                                  <p>
                                    <b>LƯU Ý!</b> Chuyển đúng số tiền hiển thị (chính xác đến 3 số
                                    cuối)
                                  </p>
                                </span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="_3KXxS _1Apjj">
                  <div className="_2gdIP">
                    <div className="_3bY-J">3</div>
                    <div className="_1grPh">Bạn đã thanh toán xong?</div>
                  </div>
                  <div className="_3IWfa">
                    <div>
                      <div className="_3TbXB">
                        <p>
                          Sau khi xác nhận số tiền thanh toán, chúng tôi sẽ gửi vé điện tử vào email
                          của bạn.
                        </p>
                        <div className="VYMcY">
                          <button className="_176J8 _3_ByF gLbQ- _2ARch" type="button">
                            Tôi đã hoàn tất thanh toán
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="dR_e4">
              <div>
                <div className="_1WLFx">
                  <div className="_25AoU">
                    <div>
                      <div className="_1EnO2">Mã đặt chỗ</div>
                      <div className="HM2HX _1dlJ1 tvat-bookingId">701557386</div>
                    </div>
                  </div>
                  <div className="_2F94N">
                    <div className="_2ZOAD">
                      <div className="Wi1hn">CHUYẾN ĐI</div>
                      <div className="o9sMy">
                        <span className="_1KrnW _1EnnQ _2HSse _1dKIX">Chi tiết</span>
                      </div>
                      <div className="_1p3Vs">Chuyến bay</div>
                      <div className="_1k1DL tvat-trip-date">09 Jun 2021</div>
                      <ul className="_3bIp9">
                        <li>Hà Nội (HAN) → TP HCM (SGN)</li>
                      </ul>
                      <div />
                    </div>
                    <div className="_1DfBz">
                      <div className="_niCk">Danh sách hành khách</div>
                      <ol className="_wHjZ">
                        <li className="YHIFZ tvat-passengerName">
                          <div className="_1Ul3t">Cô xcvbxcvb xcvbxcvb</div>
                          <div>Người lớn</div>
                        </li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};
