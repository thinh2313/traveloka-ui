import React from 'react';

export const FullSearch = () => {
  return (
    <div id="desktopContentV3" className="desktopV3">
      <div className="css-1dbjc4n r-zh076v" style={{ paddingTop: '88px' }}>
        <div className="css-1dbjc4n r-184en5c" />
        <div id="promo-banner-placeholder">
          <div className="css-1dbjc4n r-1awozwy r-lg0ffd r-kdyh1x r-1pcd2l5 r-13qz1uu" />
        </div>
        <div className="css-1dbjc4n r-13awgt0">
          <div className="css-1dbjc4n r-391gc0">
            <div className="css-1dbjc4n r-ywje51 r-ibla3m r-o4ii31">
              <div className="css-1dbjc4n r-16lk18l r-1jgb5lz r-1wzrnnt r-16k0tzm" />
              <div className="css-1dbjc4n r-1mmau9f">
                <div className="css-1dbjc4n">
                  <div className="css-1dbjc4n" />
                  <div />
                  <div className="css-1dbjc4n r-19u6a5r r-b1huai r-mhe3cw">
                    <div className="css-1dbjc4n" id="flight-ow-header">
                      <div className="css-1dbjc4n r-1ihkh82 r-kdyh1x r-da5iq2 r-1udh08x r-b1huai">
                        <div>
                          <div className="css-1dbjc4n r-1awozwy r-18u37iz">
                            <div className="css-1dbjc4n r-14lw9ot r-1wh2hl7 r-sqtsar r-o8yidv r-waaub4 r-1yos0t3 r-13awgt0 r-1d4mawv r-1pcd2l5">
                              <div className="css-1dbjc4n r-1awozwy r-9nbb9w r-ofwwxa r-1i1ao36 r-18u37iz">
                                <div className="css-1dbjc4n">
                                  <h3
                                    aria-level={3}
                                    dir="auto"
                                    role="heading"
                                    className="css-4rbku5 css-901oao r-1sixt3s r-ubezar r-b88u0q r-rjixqe r-fdjqy7"
                                    style={{ color: 'rgb(3, 18, 26)', marginBottom: '8px' }}
                                  >
                                    Hanoi (HAN) → Ho Chi Minh City (SGN)
                                  </h3>
                                  <div
                                    dir="auto"
                                    className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                    style={{ color: 'rgb(104, 113, 118)' }}
                                  >
                                    Thu, 10 Jun 2021 | 1 passengers | Economy
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="css-1dbjc4n r-1ihkh82 r-kdyh1x r-iphfwy r-f727ji r-i023vh r-tskmnb">
                              <div
                                aria-live="polite"
                                role="button"
                                tabIndex={0}
                                className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                style={{ backgroundColor: 'rgb(1, 148, 243)' }}
                              >
                                <div
                                  dir="auto"
                                  className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                >
                                  Change search
                                </div>
                                <div
                                  className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                  style={{ opacity: 1 }}
                                >
                                  <div
                                    dir="auto"
                                    aria-hidden="true"
                                    className="css-901oao r-1awozwy r-6koalj r-1d4mawv"
                                    style={{ color: 'rgb(255, 255, 255)' }}
                                  >
                                    <img
                                      src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/light/ic_system_search_24px-dee9abb8908124af6564d818120f1c08.svg"
                                      height={16}
                                      width={16}
                                    />
                                  </div>
                                  <div
                                    dir="auto"
                                    aria-hidden="true"
                                    className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                    style={{ color: 'rgb(255, 255, 255)' }}
                                  >
                                    Change search
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="css-1dbjc4n" />
                  <div>
                    <div
                      className="css-1dbjc4n r-19u6a5r r-b1huai r-184en5c"
                      style={{ position: 'relative' }}
                    >
                      <div className="css-1dbjc4n r-1ihkh82 r-1wh2hl7 r-e65k4z r-da5iq2 r-18u37iz r-1w6e6rj r-ymttw5 r-1f1sjgu r-184en5c">
                        <div className="css-1dbjc4n r-13awgt0">
                          <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj">
                            <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj">
                              <img
                                src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/darkBlue/ic_marketing_promo_badge_24px-aa4de9604f06c6ec160e0cf5d103630a.svg"
                                width={24}
                                height={24}
                              />
                              <h4
                                aria-level={4}
                                dir="auto"
                                role="heading"
                                className="css-4rbku5 css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-1cwl3u0 r-19u6a5r r-fdjqy7"
                                style={{ color: 'rgb(53, 65, 72)' }}
                              >
                                Ongoing Deals
                              </h4>
                            </div>
                            <div className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1w6e6rj r-1udh08x r-ymttw5">
                              <div
                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj"
                                style={{ transform: 'translateX(0px)' }}
                              >
                                <div
                                  tabIndex={0}
                                  className="css-1dbjc4n r-14lw9ot r-sdzlij r-1loqt21 r-1e081e0 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr"
                                  style={{ transitionDuration: '0s' }}
                                >
                                  <div
                                    dir="auto"
                                    className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                    style={{ color: 'rgb(1, 148, 243)' }}
                                  >
                                    Bamboo Hot Sale
                                  </div>
                                </div>
                                <div
                                  tabIndex={0}
                                  className="css-1dbjc4n r-14lw9ot r-sdzlij r-1loqt21 r-1jkjb r-1e081e0 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr"
                                  style={{ transitionDuration: '0s' }}
                                >
                                  <div
                                    dir="auto"
                                    className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                    style={{ color: 'rgb(1, 148, 243)' }}
                                  >
                                    Vietnam Airlines Hot Deal
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-1qhn6m8"
                              style={{ width: '64px' }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="css-1dbjc4n r-391gc0 r-1h8ys4a">
                        <div className="css-1dbjc4n r-1habvwh r-391gc0 r-18u37iz r-1wtj0ep r-edyy15 r-13qz1uu r-mhe3cw">
                          <div
                            dir="auto"
                            className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-mbgqwd r-tskmnb r-fdjqy7"
                            style={{ color: 'rgb(3, 18, 26)' }}
                          >
                            Filter:
                          </div>
                          <div className="css-1dbjc4n r-13awgt0 r-18u37iz r-1w6e6rj">
                            <div className="css-1dbjc4n r-15zivkp r-88pszg r-jtcya5 r-bnwqim r-184en5c">
                              <div
                                tabIndex={0}
                                className="css-1dbjc4n r-1awozwy r-1ihkh82 r-sdzlij r-1loqt21 r-6koalj r-18u37iz r-12rqra3 r-ymttw5 r-tskmnb r-1otgn73 r-1i6wzkk r-lrvibr"
                                style={{ transitionDuration: '0s' }}
                              >
                                <div
                                  dir="auto"
                                  className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                  style={{ color: 'rgb(1, 148, 243)' }}
                                >
                                  Transit
                                </div>
                                <div
                                  className="css-1dbjc4n r-855088 r-jjeupf r-114ovsg r-1ytx3yt r-15tvil0 r-1jkjb"
                                  style={{ borderTopColor: 'rgb(1, 148, 243)' }}
                                />
                              </div>
                            </div>
                            <div className="css-1dbjc4n r-15zivkp r-88pszg r-bnwqim r-184en5c">
                              <div
                                tabIndex={0}
                                className="css-1dbjc4n r-1awozwy r-1ihkh82 r-sdzlij r-1loqt21 r-6koalj r-18u37iz r-12rqra3 r-ymttw5 r-tskmnb r-1otgn73 r-1i6wzkk r-lrvibr"
                                style={{ transitionDuration: '0s' }}
                              >
                                <div
                                  dir="auto"
                                  className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                  style={{ color: 'rgb(1, 148, 243)' }}
                                >
                                  Time
                                </div>
                                <div
                                  className="css-1dbjc4n r-855088 r-jjeupf r-114ovsg r-1ytx3yt r-15tvil0 r-1jkjb"
                                  style={{ borderTopColor: 'rgb(1, 148, 243)' }}
                                />
                              </div>
                            </div>
                            <div className="css-1dbjc4n r-15zivkp r-88pszg r-bnwqim r-184en5c">
                              <div
                                tabIndex={0}
                                className="css-1dbjc4n r-1awozwy r-1ihkh82 r-sdzlij r-1loqt21 r-6koalj r-18u37iz r-12rqra3 r-ymttw5 r-tskmnb r-1otgn73 r-1i6wzkk r-lrvibr"
                                style={{ transitionDuration: '0s' }}
                              >
                                <div
                                  dir="auto"
                                  className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                  style={{ color: 'rgb(1, 148, 243)' }}
                                >
                                  Airline
                                </div>
                                <div
                                  className="css-1dbjc4n r-855088 r-jjeupf r-114ovsg r-1ytx3yt r-15tvil0 r-1jkjb"
                                  style={{ borderTopColor: 'rgb(1, 148, 243)' }}
                                />
                              </div>
                            </div>
                            <div className="css-1dbjc4n r-15zivkp r-88pszg r-bnwqim r-184en5c">
                              <div
                                tabIndex={0}
                                className="css-1dbjc4n r-1awozwy r-1ihkh82 r-sdzlij r-1loqt21 r-6koalj r-18u37iz r-12rqra3 r-ymttw5 r-tskmnb r-1otgn73 r-1i6wzkk r-lrvibr"
                                style={{ transitionDuration: '0s' }}
                              >
                                <div
                                  dir="auto"
                                  className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                  style={{ color: 'rgb(1, 148, 243)' }}
                                >
                                  More filters
                                </div>
                                <div
                                  className="css-1dbjc4n r-855088 r-jjeupf r-114ovsg r-1ytx3yt r-15tvil0 r-1jkjb"
                                  style={{ borderTopColor: 'rgb(1, 148, 243)' }}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-bnwqim">
                            <div
                              tabIndex={0}
                              className="css-1dbjc4n r-1awozwy r-1ihkh82 r-sdzlij r-1loqt21 r-6koalj r-18u37iz r-12rqra3 r-ymttw5 r-tskmnb r-1otgn73 r-1i6wzkk r-lrvibr"
                              style={{ transitionDuration: '0s' }}
                            >
                              <div
                                dir="auto"
                                className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                style={{ color: 'rgb(1, 148, 243)' }}
                              >
                                Sort
                              </div>
                              <div
                                className="css-1dbjc4n r-1awozwy r-sdzlij r-10ptun7 r-1777fci r-1jkjb r-1janqcz"
                                style={{ backgroundColor: 'rgb(1, 148, 243)' }}
                              >
                                <img
                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/light/ic_system_checkmark_16px-e8c592e0f6219f8d6609753bf0b1198d.svg"
                                  width={12}
                                  height={12}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="css-1dbjc4n r-1ow6zhx r-14gqq1x r-16k0tzm">
                    <div
                      className="css-1dbjc4n r-1awozwy r-vwrgms r-1c6pirg r-1i1ao36"
                      style={{ animationDuration: '200ms' }}
                    />
                  </div>
                  <div className="css-1dbjc4n r-1udh08x">
                    <div className="css-1dbjc4n r-9nbb9w r-vwrgms r-9xj7z8 r-1i1ao36 r-14gqq1x">
                      <div
                        className="css-1dbjc4n r-18u37iz"
                        style={{ transform: 'translateX(0px)' }}
                      >
                        <div className="css-1dbjc4n r-1ow6zhx r-88pszg r-16k0tzm">
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2015/12/17/1450350670702-a9dba44d3e9fe096f83ffe00d56a99ec.png?tr=q-75,w-28"
                                                alt="VietJet Air"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2015/12/17/1450350670702-a9dba44d3e9fe096f83ffe00d56a99ec.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2015/12/17/1450350670702-a9dba44d3e9fe096f83ffe00d56a99ec.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2015/12/17/1450350670702-a9dba44d3e9fe096f83ffe00d56a99ec.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              VietJet Air
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              12:20
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              14:30
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 10m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/darkBlue/ic_facilities_baggage_add_24px-df10676ee7bdb15615420319bf865811.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div
                                        dir="auto"
                                        className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-zl2h9q r-1ff274t r-142tt33"
                                        style={{ color: 'rgb(104, 113, 118)' }}
                                      >
                                        558.900 VND /pax
                                      </div>
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          554.900 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=q-75,w-28"
                                                alt="Bamboo Airways"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Bamboo Airways
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              14:10
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              16:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 5m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>20</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div
                                        dir="auto"
                                        className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-zl2h9q r-1ff274t r-142tt33"
                                        style={{ color: 'rgb(104, 113, 118)' }}
                                      >
                                        577.000 VND /pax
                                      </div>
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          567.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Codeshare (operated by Pacific)
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              17:40
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              19:45
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 5m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="8.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>0</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div
                                        dir="auto"
                                        className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-zl2h9q r-1ff274t r-142tt33"
                                        style={{ color: 'rgb(104, 113, 118)' }}
                                      >
                                        580.000 VND /pax
                                      </div>
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          576.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=q-75,w-28"
                                                alt="Bamboo Airways"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2020/02/19/1582084897287-d2de240a06eac5e3a70126425b62ee0b.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Bamboo Airways
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              10:05
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              12:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 10m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>20</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          601.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              21:00
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              23:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 15m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>23</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          624.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              07:00
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              09:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 15m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>23</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          644.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              10:00
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              12:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 15m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>23</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          644.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              15:00
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              17:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 15m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>23</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_meal_24px-c66421c72d506576cf9851e390b04a28.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                              <div className="css-1dbjc4n r-61z16t">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          644.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                          <div className="css-1dbjc4n">
                            <div tabIndex={0} className="css-1dbjc4n r-9nbb9w r-otx420 r-1i1ao36">
                              <div
                                className="css-1dbjc4n r-14lw9ot r-kdyh1x r-rs99b7 r-16k0tzm"
                                style={{ borderColor: 'rgb(247, 249, 250)' }}
                              >
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-184en5c">
                                  <div className="css-1dbjc4n r-912u2z r-htfu76 r-q3we1 r-184en5c">
                                    <div className="css-1dbjc4n">
                                      <div className="css-1dbjc4n r-18u37iz">
                                        <div
                                          className="css-1dbjc4n r-855088 r-119zq30 r-eujbse r-1w2pmg r-ws9h79 r-eqo98v r-1h2t8mc"
                                          style={{ borderTopColor: 'rgb(48, 197, 247)' }}
                                        />
                                        <div className="css-1dbjc4n" style={{ zIndex: 20 }}>
                                          <div className="css-1dbjc4n">
                                            <div
                                              className="css-1dbjc4n r-1awozwy r-j9nkxj r-o8yidv r-f727ji r-j2kj52 r-oyd9sg"
                                              style={{
                                                backgroundColor: 'rgb(48, 197, 247)',
                                                marginLeft: '0px',
                                              }}
                                            >
                                              <div
                                                dir="auto"
                                                className="css-901oao r-1sixt3s r-1enofrn r-majxgm r-1cwl3u0 r-fdjqy7"
                                                style={{ color: 'rgb(3, 18, 26)' }}
                                              >
                                                Good deal
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-r7j6xl r-114ovsg r-gg9v3n r-1w2pmg r-zchlnj r-ipm5af r-1h2t8mc"
                                          style={{ borderBottomColor: 'rgb(48, 197, 247)' }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="css-1dbjc4n r-1tuna9m r-1oszu61 r-18u37iz r-1w6e6rj r-1h0z5md r-1guathk r-1ygmrgt">
                                    <div className="css-1dbjc4n r-13awgt0">
                                      <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md">
                                        <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1b1obt9">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md">
                                            <div className="css-1dbjc4n r-1d4mawv">
                                              <img
                                                src="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28"
                                                alt="Vietnam Airlines"
                                                importance="low"
                                                loading="lazy"
                                                srcSet="https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=q-75,w-28 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-2,q-75,w-28 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/08/1557291046676-ad452147a52382b5e9e3f4994cb2a3cf.png?tr=dpr-3,q-75,w-28 3x"
                                                width={28}
                                                style={{
                                                  height: '28px',
                                                  objectFit: 'contain',
                                                  objectPosition: '50% 50%',
                                                }}
                                              />
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-eqz5dr r-1w6e6rj r-1777fci r-13hce6t r-174vidy">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-aq742g r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              Vietnam Airlines
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n r-e8mqni r-1habvwh r-13awgt0 r-18u37iz r-1w6e6rj r-1h0z5md r-15m1z73">
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              17:00
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              Hanoi (HAN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-1472mwg r-1777fci r-13hce6t r-1rw7m1n">
                                            <img
                                              src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-components/svg/16/ic_flight_route_16px-2a29aa4dcda893738069caa1c12beaad.svg"
                                              importance="low"
                                              loading="lazy"
                                              width={32}
                                              height={12}
                                              style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 50%',
                                              }}
                                            />
                                          </div>
                                          <div className="css-1dbjc4n r-1kesqaa">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              19:15
                                            </div>
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                              style={{ color: 'rgb(104, 113, 118)' }}
                                            >
                                              H. C. M. City (SGN)
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-e8mqni r-1habvwh r-1h0z5md r-15m1z73 r-w0va4e r-7bouqp">
                                            <div
                                              dir="auto"
                                              className="css-901oao r-1sixt3s r-ubezar r-majxgm r-135wba7 r-fdjqy7"
                                              style={{ color: 'rgb(3, 18, 26)' }}
                                            >
                                              2h 15m
                                            </div>
                                            <div className="css-1dbjc4n r-18u37iz r-1w6e6rj r-1h0z5md r-14gqq1x">
                                              <div
                                                dir="auto"
                                                className="css-901oao r-13awgt0 r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                                style={{ color: 'rgb(104, 113, 118)' }}
                                              >
                                                Direct
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n">
                                            <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1w6e6rj r-1h0z5md r-f9dfq4">
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width={20}
                                                  height={20}
                                                  viewBox="0 0 24 24"
                                                >
                                                  <g fill="none" fillRule="evenodd">
                                                    <rect width={24} height={24} />
                                                    <path
                                                      stroke="#0194f3"
                                                      strokeWidth={2}
                                                      d="M9,5 L15,5 C15.5522847,5 16,5.44771525 16,6 L16,8 L8,8 L8,6 C8,5.44771525 8.44771525,5 9,5 Z M4.5638852,8 L19.4361148,8 C20.3276335,8 20.6509198,8.09282561 20.9768457,8.2671327 C21.3027716,8.4414398 21.5585602,8.69722837 21.7328673,9.0231543 C21.9071744,9.34908022 22,9.67236646 22,10.5638852 L22,17.4361148 C22,18.3276335 21.9071744,18.6509198 21.7328673,18.9768457 C21.5585602,19.3027716 21.3027716,19.5585602 20.9768457,19.7328673 C20.6509198,19.9071744 20.3276335,20 19.4361148,20 L4.5638852,20 C3.67236646,20 3.34908022,19.9071744 3.0231543,19.7328673 C2.69722837,19.5585602 2.4414398,19.3027716 2.2671327,18.9768457 C2.09282561,18.6509198 2,18.3276335 2,17.4361148 L2,10.5638852 C2,9.67236646 2.09282561,9.34908022 2.2671327,9.0231543 C2.4414398,8.69722837 2.69722837,8.4414398 3.0231543,8.2671327 C3.34908022,8.09282561 3.67236646,8 4.5638852,8 Z"
                                                    />
                                                    <text
                                                      x="5.5"
                                                      y={18}
                                                      fill="#0194f3"
                                                      fontSize={11}
                                                      fontFamily="MuseoSans, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
                                                      fontWeight={900}
                                                    >
                                                      <tspan>23</tspan>
                                                    </text>
                                                  </g>
                                                </svg>
                                              </div>
                                              <div className="css-1dbjc4n r-1kb76zh">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_meal_24px-c66421c72d506576cf9851e390b04a28.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                              <div className="css-1dbjc4n r-61z16t">
                                                <img
                                                  src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/blue/ic_facilities_entertainment_24px-8ddbf131a0adec74006d5072a5ee75a4.svg"
                                                  width={20}
                                                  height={20}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="css-1dbjc4n r-1wzrnnt">
                                        <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-1h0z5md r-p1pxzi">
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Flight Details
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="css-1dbjc4n r-uia4a0 r-1awozwy r-18u37iz r-15m1z73 r-xd6kpl">
                                            <div
                                              aria-live="polite"
                                              role="button"
                                              tabIndex={0}
                                              className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                            >
                                              <div
                                                className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                                style={{ opacity: 1 }}
                                              >
                                                <div
                                                  dir="auto"
                                                  className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                                  style={{ color: 'rgb(1, 148, 243)' }}
                                                >
                                                  Fare Info
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="css-1dbjc4n">
                                          <div
                                            className="css-1dbjc4n r-92ng3h"
                                            style={{
                                              backgroundColor: 'rgb(1, 148, 243)',
                                              height: '2px',
                                              marginLeft: '0px',
                                              transform: 'scaleX(0)',
                                              transformOrigin: 'left top',
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1h0z5md r-1ow6zhx r-x1dlf0 r-1ui2xcl">
                                      <div className="css-1dbjc4n r-1awozwy r-18u37iz r-1w6e6rj r-17s6mgv r-6gpygo">
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-adyw6z r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(255, 94, 31)' }}
                                        >
                                          644.000 VND
                                        </div>
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1sixt3s r-1b43r93 r-b88u0q r-rjixqe r-fdjqy7"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          /pax
                                        </div>
                                      </div>
                                      <div
                                        aria-live="polite"
                                        role="button"
                                        tabIndex={0}
                                        className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-1e081e0 r-5njf8e r-1otgn73 r-lrvibr"
                                        style={{ backgroundColor: 'rgb(255, 94, 31)' }}
                                      >
                                        <div
                                          dir="auto"
                                          className="css-901oao r-1yadl64 r-1vonz36 r-109y4c4 r-1cis278 r-1udh08x r-t60dpp r-u8s1d r-3s2u2q r-92ng3h"
                                        >
                                          Choose
                                        </div>
                                        <div
                                          className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-1777fci"
                                          style={{ opacity: 1 }}
                                        >
                                          <div
                                            dir="auto"
                                            aria-hidden="true"
                                            className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                            style={{ color: 'rgb(255, 255, 255)' }}
                                          >
                                            Choose
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-kdyh1x r-da5iq2 r-19i43ro r-1udh08x r-1guathk r-417010">
                                  <div className="css-1dbjc4n r-1ielgck r-1ivxbpt r-1i1ao36" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="css-1dbjc4n r-tbmifm" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
