import React, { useState } from "react";

function Payment2() {
  const [choice, setChoice] = useState("");

  return (
    <div>
      <div class="_160_o">
        <div class="_3ndkf">
          <p>
            Tiến hành thanh toán trong vòng 00:10:48{" "}
            <img
              class="_2tukb"
              src="https://ik.imagekit.io/tvlk/image/imageResource/2018/07/16/1531740826838-b56ff1fbeebd63e65287af273888880d.png?tr=q-75"
            />
          </p>
        </div>
        <div>
          <div class="_1rgP7">
            <div class="_2sV2p">
              <h4>Thẻ tín dụng</h4>
              <div class="u1IoA">
                <img
                  class="Ua80U"
                  src="https://ik.imagekit.io/tvlk/image/imageResource/2017/01/17/1484655630637-0dcca3761eb5910f1835f438f153bfae.png?tr=q-75"
                />
                <img
                  class="Ua80U"
                  src="https://ik.imagekit.io/tvlk/image/imageResource/2017/01/06/1483707776912-1abb188266f6d5b3f2e27f4733ca32e9.png?tr=q-75"
                />
                <img
                  class="Ua80U"
                  src="https://ik.imagekit.io/tvlk/image/imageResource/2017/01/06/1483707787206-abc175b224ab92a6967e24bc17c30f45.png?tr=q-75"
                />
                <img
                  class="Ua80U"
                  src="https://ik.imagekit.io/tvlk/image/imageResource/2017/07/10/1499673365437-1e1522e5cc323e7e8a7b57b90e81dbc9.png?tr=q-75"
                />
              </div>
            </div>
            <div class="_38Wgm">
              <div class="_3N9us">
                Bạn có thể chuyển tiền mặt tại quầy giao dịch hoặc chuyển khoản
                qua Internet Banking và trạm ATM.
              </div>
              <div class="_1GHFI">
                <h5>Lưu ý trước khi thanh toán</h5>
                <div class="G62dK">
                  <div>
                    <ul>
                      <li>
                        Hiện tại, chúng tôi <strong>không chấp nhận</strong>{" "}
                        chuyển khoản liên ngân hàng qua ATM hoặc Internet
                        Banking.
                      </li>
                      <li>Phí chuyển khoản sẽ do người chuyển trả.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="_2Xflz _1cFo-" style={{display: "block;"}}>
                <h5 style="margin-bottom: 8px;">Chọn ngân hàng</h5>
                <div>
                  <div class="_3X2uX">
                    <div class="GMjqv">
                      <input
                        type="radio"
                        name="TRANSFER_scopes"
                        class="_3PHe6"
                        value="vietcom_VND"
                        checked=""
                      />
                      <label></label>
                    </div>
                    <div class="_37v_3">
                      <p>Vietcombank</p>
                      <div class="_2vL63">
                        <img
                          class="_1jUTI"
                          src="https://ik.imagekit.io/tvlk/image/imageResource/2017/03/20/1489981886316-4f45965c5fb8a6d379a327f0f7cfcd42.png?tr=q-75"
                        />
                      </div>
                      <div style={{clear: "both;"}}></div>
                    </div>
                  </div>
                  <div class="_3X2uX">
                    <div class="GMjqv">
                      <input
                        type="radio"
                        name="TRANSFER_scopes"
                        class="_3PHe6"
                        value="techcom_VND"
                      />
                      <label></label>
                    </div>
                    <div class="_37v_3">
                      <p>Techcombank</p>
                      <div class="_2vL63">
                        <img
                          class="_1jUTI"
                          src="https://ik.imagekit.io/tvlk/image/imageResource/2017/03/20/1489981873480-395cc8cd7703239d91a0d8f08ca0ef5e.png?tr=q-75"
                        />
                      </div>
                      <div style={{clear: "both"}}></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div></div>
          </div>
        </div>
        <div>
          <div class="_3lnxM">
            <div class="_35d8d">
              <div class="_3hSrH">
                <input type="checkbox" name="coupon" value="applyCoupon" />
                <label></label>
              </div>
              <div class="_pET7">
                <p>Nhập mã giảm giá</p>
              </div>
              <div class="_8qE1N" style={{display: "none;"}}>
                <div class="I6gUe">
                  <div>
                    <label class="aLzYS _2P9Gi _6LXde">
                      <div class="_1YJhl">
                        <div class="_315kk">
                          <div class="_1h4cK">
                            <div>
                              <input
                                class="_1nWNU"
                                placeholder="VD: CHEAPTRAVEL"
                                type="text"
                                value=""
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </label>
                  </div>
                </div>
                <button class="_1-J4N _22K0g gLbQ- _3GtsO" type="button">
                  Áp dụng mã
                </button>
                <span style={{clear: "both;"}}></span>
              </div>
            </div>
            <div style={{height: "10px;"},{ width: "100%;"}}></div>
            <div></div>
          </div>
          <div>
            <div class="wI5ly _2Xa_I">
              <div class="_23f-o">
                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2017/03/13/1489380740319-3fde02d9f22b013b40ccc9662fcb69dc.png"
                  alt="Warning"
                />
              </div>
              <div class="O3UVd"></div>
            </div>
            <div class="_3hp60">
              <h4>Chi tiết giá</h4>
              <div class="_1kEel tvat-flight_sales">
                <div class="_1Rrg4">VietJet Air (Người lớn) x 1</div>
                <div class="_3slsy">554.900 VND</div>
              </div>
              <div class="_1kEel tvat-unique_code">
                <div class="_1Rrg4">Mã khớp giao dịch</div>
                <div class="_3slsy">56 VND</div>
              </div>
              <div class="_1AgIM"></div>
              <div class="_1kEel">
                <div class="_1Rrg4">Tổng giá tiền</div>
                <div class="_3slsy _37iQp tvat-totalPrice">554.956 VND</div>
              </div>
            </div>
            <div class="nrMug _3-t4R _2Xa_I tvat-loyaltyPointInfo">
              <div>
                <div class="_2azAe">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2017/03/10/1489122331174-c315477839a88a376f3d410f6ba19861.png"
                    alt="Help"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="_3L17W">
          <p>
            <span>
              Bằng việc nhấn Thanh toán, bạn đồng ý{" "}
              <a
                target="_blank"
                href="https://www.traveloka.com/vi-vn/termsandconditions"
              >
                Điều khoản &amp; Điều kiện
              </a>{" "}
              và{" "}
              <a
                target="_blank"
                href="https://www.traveloka.com/vi-vn/privacypolicy"
              >
                Chính sách quyền riêng tư
              </a>
              .
            </span>
          </p>
          <button
            class="tvat-pay-now-button _3_ByF gLbQ- _90_75"
            id="payButton"
            type="button"
            style={{margintop: "8px;"}}
          >
            Thanh toán Chuyển khoản ngân hàng
          </button>
        </div>
      </div>
    </div>
  );
}

export default Payment2;
