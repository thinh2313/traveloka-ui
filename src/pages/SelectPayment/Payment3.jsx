import React, { useState } from "react";

function Payment3() {
  const [choice, setChoice] = useState("");

  return (
    <div>
      <div class="_160_o">
        <div class="_3ndkf">
          <p>
            Tiến hành thanh toán trong vòng 00:11:41{" "}
            <img
              class="_2tukb"
              src="https://ik.imagekit.io/tvlk/image/imageResource/2018/07/16/1531740826838-b56ff1fbeebd63e65287af273888880d.png?tr=q-75"
            />
          </p>
        </div>
        <div>
          <div class="_1rgP7">
            <div class="_2sV2p">
              <h4>Tại cửa hàng</h4>
            </div>
            <div class="_38Wgm">
              <div class="_1GHFI">
                <h5>Lưu ý trước khi thanh toán</h5>
                <div class="G62dK">
                  <div>
                    <ul>
                      <li>
                        Thanh toán phải được thực hiện theo thời hạn quy định và
                        trong khung giờ kinh doanh của cửa hàng.
                      </li>
                      <li>
                        Để tìm cửa hàng gần bạn nhất, vui lòng{" "}
                        <a
                          href="https://payoo.vn/mapv2/public/index.php?verify=true"
                          target="_blank"
                        >
                          xem tại đây
                        </a>
                        .
                      </li>
                      <li>
                        Bạn có thể tiến hành thanh toán tại một trong các cửa
                        hàng sau:
                      </li>
                    </ul>
                  </div>
                  <img
                    class="_2j-dc"
                    src="https://ik.imagekit.io/tvlk/image/imageResource/2018/08/15/1534319429277-7db9dcfaae850bda5d0630499079c303.png?tr=q-75"
                  />
                </div>
              </div>
              <div class="_2Xflz _1cFo-" style={{display: "none;"}}></div>
            </div>
            <div></div>
          </div>
        </div>
        <div>
          <div class="_3lnxM">
            <div class="_35d8d">
              <div class="_3hSrH">
                <input type="checkbox" name="coupon" value="applyCoupon" />
                <label></label>
              </div>
              <div class="_pET7">
                <p>Nhập mã giảm giá</p>
              </div>
              <div class="_8qE1N" style={{display: "none;"}}>
                <div class="I6gUe">
                  <div>
                    <label class="aLzYS _2P9Gi _6LXde">
                      <div class="_1YJhl">
                        <div class="_315kk">
                          <div class="_1h4cK">
                            <div>
                              <input
                                class="_1nWNU"
                                placeholder="VD: CHEAPTRAVEL"
                                type="text"
                                value=""
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </label>
                  </div>
                </div>
                <button class="_1-J4N _22K0g gLbQ- _3GtsO" type="button">
                  Áp dụng mã
                </button>
                <span style={{clear:" both;"}}></span>
              </div>
            </div>
            <div style={{height: "10px;"}, {width: "100%;"}}></div>
            <div></div>
          </div>
          <div>
            <div class="wI5ly _2Xa_I">
              <div class="_23f-o">
                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2017/03/13/1489380740319-3fde02d9f22b013b40ccc9662fcb69dc.png"
                  alt="Warning"
                />
              </div>
              <div class="O3UVd"></div>
            </div>
            <div class="_3hp60">
              <h4>Chi tiết giá</h4>
              <div class="_1kEel tvat-flight_sales">
                <div class="_1Rrg4">VietJet Air (Người lớn) x 1</div>
                <div class="_3slsy">554.900 VND</div>
              </div>
              <div class="_1AgIM"></div>
              <div class="_1kEel">
                <div class="_1Rrg4">Tổng giá tiền</div>
                <div class="_3slsy _37iQp tvat-totalPrice">554.900 VND</div>
              </div>
            </div>
            <div class="nrMug _3-t4R _2Xa_I tvat-loyaltyPointInfo">
              <div>
                <div class="_2azAe">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2017/03/10/1489122331174-c315477839a88a376f3d410f6ba19861.png"
                    alt="Help"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="_3L17W">
          <p>
            <span>
              Bằng việc nhấn Thanh toán, bạn đồng ý{" "}
              <a
                target="_blank"
                href="https://www.traveloka.com/vi-vn/termsandconditions"
              >
                Điều khoản &amp; Điều kiện
              </a>{" "}
              và{" "}
              <a
                target="_blank"
                href="https://www.traveloka.com/vi-vn/privacypolicy"
              >
                Chính sách quyền riêng tư
              </a>
              .
            </span>
          </p>
          <button
            class="tvat-pay-now-button _3_ByF gLbQ- _90_75"
            id="payButton"
            type="button"
            style={{margintop: "8px;"}}
          >
            Thanh toán Tại cửa hàng
          </button>
        </div>
      </div>
    </div>
  );
}

export default Payment3;
