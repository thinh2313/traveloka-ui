import React from 'react';
import Payment1 from "./Payment1";
import Paymentselecttab from "./Paymentselecttab";
import Panel from "../Tabs/Panel";
import Payment2 from "./Payment2";
import"./Selectpay.css";
import Payment3 from "./Payment3";
import Payment4 from "./Payment4";

function Paymentform ()  {
  
    return (
      <div>
        
          <Paymentselecttab className="Tabs" /*tab cha*/>
            <Panel title="Thẻ tín dụng" className="Panel" /*tabs con*/>
              <div className="content-pay" /*Hiển thị nội dung tabs con */>
                <Payment1 />
              </div>
            </Panel>
            <Panel title="Chuyển khoản ngân hàng" className="Panel">
              <div className="content-pay">
                <Payment2 />
              </div>
            </Panel>
            <Panel title="Tại cửa hàng" className="Panel">
              <div className="content-pay">
                <Payment3 />
              </div>
            </Panel>
            <Panel title="Thẻ ATM nội địa" className="Panel">
              <div className="content-pay">
                <Payment4 />
              </div>
            </Panel>
          </Paymentselecttab>
        </div>
       
    );
  
}

export default Paymentform;
