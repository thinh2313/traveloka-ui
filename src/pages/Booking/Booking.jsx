import React from 'react';


export const Booking = () => {
  return (
    <div className="home-page">
      <div>
        <div className="_2XbV5" />
        <div>
          <div>
            <div className="_2XbV5">
              <div className="_6gZ6t">
                <h2 className="_2vvS_ _1YLVD">Đặt chỗ của tôi</h2>
                <div className="_1AkoG">
                  <span className="_1KrnW jjGhl Edww1 _1ABp6">
                    Điền thông tin và xem lại đặt chỗ.
                  </span>
                </div>
                <div className="_1Q04B">
                  <div className="zhgVB">
                    <div className="zhgVB">
                      <div>
                        <div>
                          <div>
                            <div>
                              <div className="mMmI2 _2JlHX _284wg lLNFQ" data-elevation={1}>
                                <div className="mMcrn _1E1LD ">
                                  <img
                                    width={100}
                                    height={100}
                                    src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/web-ui/shared/images/LoginBenefits-296b03a171e13f7eb131dd83e7ee6c21.png"
                                  />
                                </div>
                                <div className="mMcrn ZZMFk">
                                  <div className="_36OBw">
                                    <div>
                                      <span className="_1KrnW jjGhl _2HSse _1dKIX">
                                        Đăng nhập hoặc Đăng ký và tận hưởng ưu đãi dành riêng cho
                                        thành viên
                                      </span>
                                    </div>
                                  </div>
                                  <div className="_2BklA lLNFQ">
                                    <div className="_3zrOU mMcrn">
                                      <span className="_1KrnW _1EnnQ Edww1 _28SNp">
                                        <svg
                                          strokeWidth={0}
                                          fill="#8f8f8f"
                                          className="_3GhTG"
                                          height={24}
                                          stroke="currentColor"
                                          strokeLinecap="round"
                                          viewBox="0 0 24 24"
                                          width={24}
                                          xmlns="http://www.w3.org/2000/svg"
                                          xmlnsXlink="http://www.w3.org/1999/xlink"
                                        >
                                          <g>
                                            <path d="M5.20232827,18.0234356 C5.09905338,18.5627772 4.57446231,19 4.02041571,19 L2.02746439,19 C1.51770129,19 1.08952925,18.6165518 1.03351899,18.1098751 C1.01122542,17.9082044 1,17.7046911 1,17.5 C1,14.4624339 3.46243388,12 6.5,12 C7.44735991,12 8.33877676,12.2395207 9.11699612,12.6613078 C8.41635675,13.000418 7.77833889,13.4593406 7.22483322,14.0131229 C6.90373072,14.0044346 6.60283047,14 6.5,14 C4.73676405,14 3.27805926,15.3038529 3.03544443,17 L5.39829904,17 L5.20232827,18.0234356 Z M3,8.0093689 C3,6.9010286 3.88890504,6 4.99895656,6 L6.00104344,6 C7.1089877,6 8,6.89502771 8,8.0093689 L8,9.49916726 C8,10.8816211 6.88521447,12 5.5,12 C4.12075457,12 3,10.8752171 3,9.49916726 L3,8.0093689 Z M6.00104344,8 L4.99917107,8 C4.99967743,8.00004409 5,8.00074211 5,8.0093689 L5,9.49916726 C5,9.77225073 5.2269336,10 5.5,10 C5.77920157,10 6,9.77848985 6,9.49916726 L6,8.0093689 C6,7.9975891 6.00240008,8 6.00104344,8 Z M18.7976717,18.0234356 L18.601701,17 L20.9645556,17 C20.7219407,15.3038529 19.263236,14 17.5,14 C17.3971695,14 17.0962693,14.0044346 16.7751668,14.0131229 C16.2216611,13.4593406 15.5836432,13.000418 14.8830039,12.6613078 C15.6612232,12.2395207 16.5526401,12 17.5,12 C20.5375661,12 23,14.4624339 23,17.5 C23,17.7046911 22.9887746,17.9082044 22.966481,18.1098751 C22.9104707,18.6165518 22.4822987,19 21.9725356,19 L19.9795843,19 C19.4255377,19 18.9009466,18.5627772 18.7976717,18.0234356 Z M16,8.0093689 C16,6.9010286 16.888905,6 17.9989566,6 L19.0010434,6 C20.1089877,6 21,6.89502771 21,8.0093689 L21,9.49916726 C21,10.8816211 19.8852145,12 18.5,12 C17.1207546,12 16,10.8752171 16,9.49916726 L16,8.0093689 Z M19.0010434,8 L17.9991711,8 C17.9996774,8.00004409 18,8.00074211 18,8.0093689 L18,9.49916726 C18,9.77225073 18.2269336,10 18.5,10 C18.7792016,10 19,9.77848985 19,9.49916726 L19,8.0093689 C19,7.9975891 19.0024001,8 19.0010434,8 Z M18,19.5 C18,19.6949445 17.9920635,19.88896 17.976278,20.081649 C17.9337693,20.6005376 17.5002437,21 16.9796168,21 L7.02038319,21 C6.49975635,21 6.06623073,20.6005376 6.02372205,20.081649 C6.00793646,19.88896 6,19.6949445 6,19.5 C6,15.9302914 8.66390125,13 12,13 C15.3360987,13 18,15.9302914 18,19.5 Z M8.02435098,19 L15.975649,19 C15.7534636,16.7343501 14.039116,15 12,15 C9.96088399,15 8.24653641,16.7343501 8.02435098,19 Z M8,7.00591905 C8,5.34770262 9.34407076,4 10.9979131,4 L13.0020869,4 C14.6617741,4 16,5.33825574 16,7.00591905 L16,8.99074554 C16,11.2033134 14.2074485,13 12,13 C9.79086701,13 8,11.2026266 8,8.99074554 L8,7.00591905 Z M10,7.00591905 L10,8.99074554 C10,10.0996775 10.8970634,11 12,11 C13.1018461,11 14,10.0997742 14,8.99074554 L14,7.00591905 C14,6.44281524 13.5571946,6 13.0020869,6 L10.9979131,6 C10.4498502,6 10,6.45106577 10,7.00591905 Z" />
                                          </g>
                                        </svg>
                                        Đặt chỗ nhanh và dễ dàng hơn với Passenger Quick Pick
                                      </span>
                                    </div>
                                  </div>
                                  <a>
                                    <div>
                                      <span className="_1KrnW _1EnnQ _2HSse">
                                        Đăng nhập hoặc Đăng ký
                                      </span>
                                    </div>
                                  </a>
                                </div>
                              </div>
                              <div className="zYvfE">
                                <div className="VJPQJ">
                                  <div className="_2_D95">
                                    <div className="_3sM-4">
                                      <div className="_2AKT2 _2TAe1">
                                        <div>
                                          <div className="_2Qki3 _2r7mX">
                                            <svg
                                              viewBox="0 0 25 25"
                                              fill="#434343"
                                              height={24}
                                              stroke="currentColor"
                                              strokeLinecap="round"
                                              width={24}
                                              xmlns="http://www.w3.org/2000/svg"
                                              xmlnsXlink="http://www.w3.org/1999/xlink"
                                            >
                                              <g transform="translate(1.000000, 1.000000)">
                                                <g
                                                  stroke="none"
                                                  strokeWidth={1}
                                                  fill="none"
                                                  fillRule="evenodd"
                                                >
                                                  <g fill="#434343">
                                                    <g>
                                                      <path d="M12,13.4142136 L5.70710678,19.7071068 C5.31658249,20.0976311 4.68341751,20.0976311 4.29289322,19.7071068 C3.90236893,19.3165825 3.90236893,18.6834175 4.29289322,18.2928932 L10.5893105,12.003524 L4.29289322,5.70710678 C3.90236893,5.31658249 3.90236893,4.68341751 4.29289322,4.29289322 C4.68341751,3.90236893 5.31658249,3.90236893 5.70710678,4.29289322 L12,10.5857864 L18.2928932,4.29289322 C18.6834175,3.90236893 19.3165825,3.90236893 19.7071068,4.29289322 C20.0976311,4.68341751 20.0976311,5.31658249 19.7071068,5.70710678 L13.4180748,11.9961388 L19.7071068,18.2928932 C20.0976311,18.6834175 20.0976311,19.3165825 19.7071068,19.7071068 C19.3165825,20.0976311 18.6834175,20.0976311 18.2928932,19.7071068 L12,13.4142136 L12,13.4142136 Z" />
                                                    </g>
                                                  </g>
                                                </g>
                                              </g>
                                            </svg>
                                          </div>
                                          <div>
                                            <div className="_29hyg">
                                              <div>
                                                <div>
                                                  <div className="lyY7- _3llKY">
                                                    <div />
                                                    <div className="register-pop--top">
                                                      <h1>Đăng nhập tài khoản</h1>
                                                      <div className="_2h7qe">
                                                        <div className>
                                                          <label className="GmGpq">
                                                            Email hoặc số di động
                                                          </label>
                                                          <input
                                                            type="text"
                                                            name="username"
                                                            data-id="usernameField"
                                                          />
                                                          <div className>
                                                            <div className="_2s99A _2D3rM" />
                                                            <div className="_2D3rM" />
                                                          </div>
                                                        </div>
                                                        <br />
                                                        <div className>
                                                          <label className="GmGpq">Mật khẩu</label>
                                                          <input
                                                            type="password"
                                                            name="password"
                                                            data-id="passwordField"
                                                          />
                                                          <div className>
                                                            <div className="_2s99A _2D3rM" />
                                                            <div className="_2D3rM" />
                                                          </div>
                                                        </div>
                                                        <div className="E8rMA">
                                                          <a
                                                            href="https://www.traveloka.com/vi-vn/forgotpassword"
                                                            target="_blank"
                                                            id="linkForgot"
                                                          >
                                                            Quên mật khẩu
                                                          </a>
                                                        </div>
                                                      </div>
                                                      <div>
                                                        <button
                                                          className="_3n-wd _3_ByF gLbQ- _1AdOq _90_75"
                                                          type="button"
                                                          data-id="submitBtn"
                                                        >
                                                          Đăng Nhập
                                                        </button>
                                                      </div>
                                                      <p className="_1OKZT">
                                                        <span>Bạn chưa có tài khoản? </span>
                                                        <a href="#">Đăng ký</a>
                                                      </p>
                                                    </div>
                                                    <div />
                                                  </div>
                                                  <div className="_1nv7Z">
                                                    <div className="register-pop--social">
                                                      <span>
                                                        <div
                                                          className="mMmI2 E32Rd _2gTsp"
                                                          data-elevation={1}
                                                        >
                                                          <div className="_2XtJs">
                                                            <svg
                                                              strokeWidth={0}
                                                              fill="none"
                                                              height={24}
                                                              stroke="currentColor"
                                                              strokeLinecap="round"
                                                              viewBox="0 0 24 24"
                                                              width={24}
                                                              xmlns="http://www.w3.org/2000/svg"
                                                              xmlnsXlink="http://www.w3.org/1999/xlink"
                                                              style={{
                                                                background: 'rgb(59, 89, 152)',
                                                              }}
                                                            >
                                                              <path
                                                                fill="#fff"
                                                                d="M7,8.60454545 L9.35682098,8.60454545 L9.35682098,6.29545455 C9.35682098,5.27727273 9.38385907,3.70454545 10.1138878,2.73181818 C10.8889799,1.7 11.9479721,1 13.7730438,1 C16.7472347,1 18,1.42727273 18,1.42727273 L17.4096682,4.95 C17.4096682,4.95 16.4272839,4.66363636 15.5124949,4.66363636 C14.5977059,4.66363636 13.7730438,4.99545455 13.7730438,5.91818182 L13.7730438,8.60454545 L17.5313396,8.60454545 L17.2744777,12.0454545 L13.7775502,12.0454545 L13.7775502,24 L9.35682098,24 L9.35682098,12.0454545 L7,12.0454545 L7,8.60454545 Z"
                                                              />
                                                            </svg>
                                                          </div>
                                                          <div className="vi0ep Qr5W9">
                                                            Liên kết với Facebook
                                                          </div>
                                                        </div>
                                                        <div
                                                          className="mMmI2 E32Rd _3m4bQ"
                                                          data-elevation={1}
                                                        >
                                                          <div className="_2XtJs">
                                                            <svg
                                                              viewBox="0 0 50 50"
                                                              strokeWidth={0}
                                                              fill="#FFFFFF"
                                                              stroke="none"
                                                              height={24}
                                                              strokeLinecap="round"
                                                              width={24}
                                                              xmlns="http://www.w3.org/2000/svg"
                                                              xmlnsXlink="http://www.w3.org/1999/xlink"
                                                            >
                                                              <path
                                                                d="M45,1H5C2.8,1,1,2.8,1,5v40c0,2.2,1.8,4,4,4h40c2.2,0,4-1.8,4-4V5C49,2.8,47.2,1,45,1z"
                                                                fill="#FFFFFF"
                                                              />
                                                              <g>
                                                                <g>
                                                                  <path
                                                                    d="M20.3,10.5c3.3-1.1,7-1.1,10.3,0.1c1.8,0.7,3.5,1.7,4.9,3.1c-0.5,0.5-1,1-1.5,1.5    c-0.9,0.9-1.9,1.9-2.8,2.8c-0.9-0.9-2.1-1.6-3.3-1.9c-1.4-0.4-3-0.5-4.5-0.2c-1.7,0.4-3.3,1.3-4.6,2.5c-1,1-1.8,2.3-2.2,3.6    c-1.7-1.3-3.3-2.6-5-3.9C13.4,14.6,16.6,11.8,20.3,10.5z"
                                                                    fill="#EA4335"
                                                                  />
                                                                </g>
                                                                <g>
                                                                  <path
                                                                    d="M10.3,22c0.3-1.3,0.7-2.6,1.3-3.9c1.7,1.3,3.3,2.6,5,3.9c-0.7,1.9-0.7,4,0,6c-1.7,1.3-3.3,2.6-5,3.9    C10.1,28.8,9.6,25.3,10.3,22z"
                                                                    fill="#FBBC05"
                                                                  />
                                                                </g>
                                                                <g>
                                                                  <path
                                                                    d="M25.3,22.1c4.8,0,9.6,0,14.4,0c0.5,2.7,0.4,5.5-0.4,8.1c-0.7,2.4-2,4.6-3.9,6.4c-1.6-1.3-3.2-2.5-4.9-3.8    c1.6-1.1,2.7-2.8,3.1-4.7c-2.8,0-5.6,0-8.3,0C25.3,26.1,25.3,24.1,25.3,22.1z"
                                                                    fill="#4285F4"
                                                                  />
                                                                </g>
                                                                <g>
                                                                  <path
                                                                    d="M11.6,31.9c1.7-1.3,3.3-2.6,5-3.9c0.6,1.9,1.9,3.6,3.5,4.7c1,0.7,2.2,1.2,3.4,1.5c1.2,0.2,2.4,0.2,3.7,0    c1.2-0.2,2.4-0.7,3.4-1.3c1.6,1.3,3.2,2.5,4.9,3.8c-1.8,1.6-3.9,2.7-6.3,3.3c-2.6,0.6-5.3,0.6-7.8-0.1c-2-0.5-3.9-1.5-5.6-2.8    C14.1,35.6,12.6,33.8,11.6,31.9z"
                                                                    fill="#34A853"
                                                                  />
                                                                </g>
                                                              </g>
                                                            </svg>
                                                          </div>
                                                          <div className="vi0ep">
                                                            Liên kết với Google
                                                          </div>
                                                        </div>
                                                        <div />
                                                      </span>
                                                    </div>
                                                  </div>
                                                  <div>
                                                    <div className="zYvfE">
                                                      <div className="VJPQJ">
                                                        <div className="_2_D95">
                                                          <div className="_3sM-4">
                                                            <div className="_2AKT2">
                                                              <div>
                                                                <div className="_2Qki3">
                                                                  <svg
                                                                    viewBox="0 0 25 25"
                                                                    fill="#434343"
                                                                    height={24}
                                                                    stroke="currentColor"
                                                                    strokeLinecap="round"
                                                                    width={24}
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    xmlnsXlink="http://www.w3.org/1999/xlink"
                                                                  >
                                                                    <g transform="translate(1.000000, 1.000000)">
                                                                      <g
                                                                        stroke="none"
                                                                        strokeWidth={1}
                                                                        fill="none"
                                                                        fillRule="evenodd"
                                                                      >
                                                                        <g fill="#434343">
                                                                          <g>
                                                                            <path d="M12,13.4142136 L5.70710678,19.7071068 C5.31658249,20.0976311 4.68341751,20.0976311 4.29289322,19.7071068 C3.90236893,19.3165825 3.90236893,18.6834175 4.29289322,18.2928932 L10.5893105,12.003524 L4.29289322,5.70710678 C3.90236893,5.31658249 3.90236893,4.68341751 4.29289322,4.29289322 C4.68341751,3.90236893 5.31658249,3.90236893 5.70710678,4.29289322 L12,10.5857864 L18.2928932,4.29289322 C18.6834175,3.90236893 19.3165825,3.90236893 19.7071068,4.29289322 C20.0976311,4.68341751 20.0976311,5.31658249 19.7071068,5.70710678 L13.4180748,11.9961388 L19.7071068,18.2928932 C20.0976311,18.6834175 20.0976311,19.3165825 19.7071068,19.7071068 C19.3165825,20.0976311 18.6834175,20.0976311 18.2928932,19.7071068 L12,13.4142136 L12,13.4142136 Z" />
                                                                          </g>
                                                                        </g>
                                                                      </g>
                                                                    </g>
                                                                  </svg>
                                                                </div>
                                                                <div>
                                                                  <div className="_29hyg">
                                                                    <div className="tvat-user-otp-recipients-container _21-yY">
                                                                      <h2 className="_2vvS_">
                                                                        Yêu cầu xác thực
                                                                      </h2>
                                                                      <p>
                                                                        Chúng tôi nhận thấy bạn đang
                                                                        sử dụng thiết bị mới. Do đó,
                                                                        bạn cần phải xác thực tài
                                                                        khoản. Vui lòng chọn hình
                                                                        thức nhận mã xác thực qua:
                                                                      </p>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <div className="_21o08">
                                                              <div className="_1Koj6 l9JwO">1</div>
                                                              <div className="l9JwO">2</div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div />
                                                  </div>
                                                </div>
                                                <div />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <h3 className="_2pVrN _2xTe7">Thông tin liên hệ</h3>
                    <div className="zhgVB">
                      <div>
                        <div>
                          <div className="mMmI2 _2JlHX _2XUY3 _2beDj" data-elevation={1}>
                            <div>
                              <div className="lLNFQ _3HMVE Y-rue _3iHsI">
                                <div className="mMcrn">
                                  <h4 className="_2rus_">Thông tin liên hệ</h4>
                                </div>
                                <div className="_3J_jq">
                                  <span
                                    className="_1KrnW jjGhl Edww1"
                                    style={{ color: 'rgb(7, 112, 205)' }}
                                  >
                                    Điền thông tin
                                  </span>
                                </div>
                              </div>
                              <hr className="_2si0n" data-inset="false" />
                              <div>
                                <div className>
                                  <div>
                                    <div
                                      className="SimpleFormtravelerForm"
                                      style={{
                                        margin: '0px',
                                        padding: '16px',
                                        backgroundColor: 'rgb(255, 255, 255)',
                                        borderRadius: '0px 0px 4px 4px',
                                      }}
                                    >
                                      <div>
                                        <input type="hidden" defaultValue />
                                        <div
                                          className="vdInputField vdInputFieldnamelast"
                                          style={{ marginBottom: '16px' }}
                                        >
                                          <div className>
                                            <label className="GmGpq">
                                              Họ (vd: Nguyen)<span className="_1ylAf">*</span>
                                            </label>
                                            <div className="_1VnvT _3v6Dr">
                                              <div>
                                                <label className="aLzYS _2P9Gi _6LXde">
                                                  <div className="_1YJhl">
                                                    <div className="_315kk">
                                                      <div className="_1h4cK">
                                                        <div>
                                                          <input
                                                            className="_1nWNU"
                                                            autoComplete="vd-input-field"
                                                            data-id="vdInputFieldnamelast"
                                                            type="text"
                                                            defaultValue=""
                                                          />
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </label>
                                              </div>
                                              <div />
                                            </div>
                                            <div className>
                                              <div className="_2s99A _2D3rM" />
                                              <div className="_2D3rM">
                                                như trên CMND (không dấu)
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="vdInputField vdInputFieldnamefirst"
                                          style={{ marginBottom: '16px' }}
                                        >
                                          <div className>
                                            <label className="GmGpq">
                                              Tên Đệm &amp; Tên (vd: Thi Ngoc Anh)
                                              <span className="_1ylAf">*</span>
                                            </label>
                                            <div className="_1VnvT _3v6Dr">
                                              <div>
                                                <label className="aLzYS _2P9Gi _6LXde">
                                                  <div className="_1YJhl">
                                                    <div className="_315kk">
                                                      <div className="_1h4cK">
                                                        <div>
                                                          <input
                                                            className="_1nWNU"
                                                            autoComplete="vd-input-field"
                                                            data-id="vdInputFieldnamefirst"
                                                            type="text"
                                                            defaultValue=""
                                                          />
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </label>
                                              </div>
                                              <div />
                                            </div>
                                            <div className>
                                              <div className="_2s99A _2D3rM" />
                                              <div className="_2D3rM">
                                                như trên CMND (không dấu)
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <input
                                          type="hidden"
                                          defaultValue="^[a-zA-Z ]*[a-zA-Z]+[a-zA-Z ]*$"
                                        />
                                        <div
                                          className="vdPhoneNumberField vdPhoneNumberFieldphoneNumber"
                                          style={{ marginBottom: '16px' }}
                                        >
                                          <div className>
                                            <label className="GmGpq">
                                              Điện thoại di động<span className="_1ylAf">*</span>
                                            </label>
                                            <div className="_2uESJ">
                                              <div className="input-form-phone">
                                                <div
                                                  className="_34r8b _3-qgg"
                                                  data-id="phonePrefix"
                                                >
                                                  <div className="_3mf_H">
                                                    <div className="_2p9_i">
                                                      <div className="_1WlFv _1pXlI" />
                                                      <span>+84</span>
                                                    </div>
                                                    <div className="_3ZwCM" />
                                                  </div>
                                                </div>
                                                <div className="xSZ_Q">
                                                  <div>
                                                    <label className="aLzYS _2P9Gi _6LXde">
                                                      <div className="_1YJhl">
                                                        <div className="_315kk">
                                                          <div className="_1h4cK">
                                                            <div>
                                                              <input
                                                                className="S2Fe- _3-qgg _1nWNU"
                                                                countryinfos="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]"
                                                                phonenumber={765828108}
                                                                phonecountryprefix={84}
                                                                tvlocale="[object Object]"
                                                                autoComplete="vd-phone-number-field"
                                                                dataphoneprefixid="phonePrefix"
                                                                dataphoneinputprefixid="prefixInput"
                                                                data-id="vdPhoneNumberFieldphoneNumber"
                                                                type="text"
                                                                defaultValue={765828108}
                                                              />
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div className>
                                              <div className="_2s99A _2D3rM" />
                                              <div className="_2D3rM">
                                                VD: +84 901234567 trong đó (+84) là mã quốc gia và
                                                901234567 là số di động
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="vdInputField vdInputFieldemailAddress"
                                          style={{ marginBottom: '16px' }}
                                        >
                                          <div className>
                                            <label className="GmGpq">
                                              Email<span className="_1ylAf">*</span>
                                            </label>
                                            <div className="_1VnvT _3v6Dr">
                                              <div>
                                                <label className="aLzYS _2P9Gi _6LXde">
                                                  <div className="_1YJhl">
                                                    <div className="_315kk">
                                                      <div className="_1h4cK">
                                                        <div>
                                                          <input
                                                            className="_1nWNU"
                                                            autoComplete="vd-input-field"
                                                            data-id="vdInputFieldemailAddress"
                                                            type="text"
                                                            defaultValue=""
                                                          />
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </label>
                                              </div>
                                              <div />
                                            </div>
                                            <div className>
                                              <div className="_2s99A _2D3rM" />
                                              <div className="_2D3rM">VD: email@example.com</div>
                                            </div>
                                          </div>
                                        </div>
                                        <input type="hidden" defaultValue />
                                        <input type="hidden" defaultValue />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zhgVB">
                      <div>
                        <h3 className="_2pVrN _2xTe7">Thông tin hành khách</h3>
                        <div>
                          <div data-id="adult-0">
                            <div>
                              <div className="mMmI2 zhgVB _2JlHX _2XUY3 _2beDj" data-elevation={1}>
                                <div>
                                  <div className="lLNFQ _3HMVE Y-rue _3iHsI">
                                    <div className="mMcrn">
                                      <h4 className="_2rus_">Người lớn 1</h4>
                                    </div>
                                    <div className="_3J_jq">
                                      <span
                                        className="_1KrnW jjGhl Edww1"
                                        style={{ color: 'rgb(7, 112, 205)' }}
                                      >
                                        Điền thông tin
                                      </span>
                                    </div>
                                  </div>
                                  <hr className="_2si0n" data-inset="false" />
                                  <div>
                                    <div className>
                                      <div>
                                        <div
                                          className="SimpleFormtravelerForm"
                                          style={{
                                            margin: '0px',
                                            padding: '16px',
                                            backgroundColor: 'rgb(255, 255, 255)',
                                            borderRadius: '0px 0px 4px 4px',
                                          }}
                                        >
                                          <div>
                                            <div
                                              className="j_AtE vdParagraph vdParagraphp1"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <p style={{ color: 'rgb(255, 165, 0)' }}>
                                                Tên không dấu (Đệm Tên Họ, VD: Thi Ngoc Anh Nguyen)
                                              </p>
                                            </div>
                                            <div
                                              className="vdSelectField vdSelectFieldtitle"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <div className>
                                                <label className="GmGpq">
                                                  Danh xưng<span className="_1ylAf">*</span>
                                                </label>
                                                <div className="_1VnvT _3v6Dr">
                                                  <div>
                                                    <label className="aLzYS _2P9Gi _6LXde">
                                                      <div className="_1YJhl">
                                                        <div className="_315kk">
                                                          <div className="_1h4cK">
                                                            <div>
                                                              <input
                                                                className="_3v6Dr _1nWNU"
                                                                autoComplete="vd-select-field"
                                                                data-id="vdSelectFieldtitle"
                                                                type="text"
                                                                defaultValue
                                                              />
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </label>
                                                  </div>
                                                  <div />
                                                </div>
                                                <div className>
                                                  <div className="_2s99A _2D3rM" />
                                                  <div className="_2D3rM" />
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                              className="vdInputField vdInputFieldnamelast"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <div className>
                                                <label className="GmGpq">
                                                  Họ (vd: Nguyen)<span className="_1ylAf">*</span>
                                                </label>
                                                <div className="_1VnvT _3v6Dr">
                                                  <div>
                                                    <label className="aLzYS _2P9Gi _6LXde">
                                                      <div className="_1YJhl">
                                                        <div className="_315kk">
                                                          <div className="_1h4cK">
                                                            <div>
                                                              <input
                                                                className="_1nWNU"
                                                                autoComplete="vd-input-field"
                                                                data-id="vdInputFieldnamelast"
                                                                type="text"
                                                                defaultValue
                                                              />
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </label>
                                                  </div>
                                                  <div />
                                                </div>
                                                <div className>
                                                  <div className="_2s99A _2D3rM" />
                                                  <div className="_2D3rM">
                                                    như trên CMND (không dấu)
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                              className="vdInputField vdInputFieldnamefirst"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <div className>
                                                <label className="GmGpq">
                                                  Tên Đệm &amp; Tên (vd: Thi Ngoc Anh)
                                                  <span className="_1ylAf">*</span>
                                                </label>
                                                <div className="_1VnvT _3v6Dr">
                                                  <div>
                                                    <label className="aLzYS _2P9Gi _6LXde">
                                                      <div className="_1YJhl">
                                                        <div className="_315kk">
                                                          <div className="_1h4cK">
                                                            <div>
                                                              <input
                                                                className="_1nWNU"
                                                                autoComplete="vd-input-field"
                                                                data-id="vdInputFieldnamefirst"
                                                                type="text"
                                                                defaultValue
                                                              />
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </label>
                                                  </div>
                                                  <div />
                                                </div>
                                                <div className>
                                                  <div className="_2s99A _2D3rM" />
                                                  <div className="_2D3rM">
                                                    như trên CMND (không dấu)
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <input
                                              type="hidden"
                                              defaultValue="^[a-zA-Z ]*[a-zA-Z]+[a-zA-Z ]*$"
                                            />
                                            <div
                                              className="vdDateField vdDateFieldbirthDate"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <div className>
                                                <label className="GmGpq">
                                                  Ngày sinh<span className="_1ylAf">*</span>
                                                </label>
                                                <div
                                                  data-id="vdDateFieldbirthDate"
                                                  className="VubFU"
                                                >
                                                  <div className="_23MHd">
                                                    <div className="_1VnvT _3v6Dr">
                                                      <div>
                                                        <label className="aLzYS _2P9Gi _6LXde">
                                                          <div className="_1YJhl">
                                                            <div className="_315kk">
                                                              <div className="_1h4cK">
                                                                <div>
                                                                  <input
                                                                    className="_3v6Dr _1nWNU"
                                                                    autoComplete="vd-date-field"
                                                                    data-id="day"
                                                                    type="text"
                                                                    defaultValue
                                                                  />
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </label>
                                                      </div>
                                                      <div />
                                                    </div>
                                                  </div>
                                                  <div className="_18zbl">
                                                    <div className="_1VnvT _3v6Dr">
                                                      <div>
                                                        <label className="aLzYS _2P9Gi _6LXde">
                                                          <div className="_1YJhl">
                                                            <div className="_315kk">
                                                              <div className="_1h4cK">
                                                                <div>
                                                                  <input
                                                                    className="_3v6Dr _1nWNU"
                                                                    autoComplete="vd-date-field"
                                                                    data-id="month"
                                                                    type="text"
                                                                    defaultValue
                                                                  />
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </label>
                                                      </div>
                                                      <div />
                                                    </div>
                                                  </div>
                                                  <div className="_31p1l">
                                                    <div className="_1VnvT _3v6Dr">
                                                      <div>
                                                        <label className="aLzYS _2P9Gi _6LXde">
                                                          <div className="_1YJhl">
                                                            <div className="_315kk">
                                                              <div className="_1h4cK">
                                                                <div>
                                                                  <input
                                                                    className="_3v6Dr _1nWNU"
                                                                    autoComplete="vd-date-field"
                                                                    data-id="year"
                                                                    type="text"
                                                                    defaultValue
                                                                  />
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </label>
                                                      </div>
                                                      <div />
                                                    </div>
                                                  </div>
                                                </div>
                                                <div className>
                                                  <div className="_2s99A _2D3rM" />
                                                  <div className="_2D3rM">
                                                    Hành khách người lớn (trên 12 tuổi)
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                              className="vdCountryField vdCountryFieldnationality"
                                              style={{ marginBottom: '16px' }}
                                            >
                                              <div className>
                                                <label className="GmGpq">
                                                  Quốc tịch<span className="_1ylAf">*</span>
                                                </label>
                                                <div className="_1VnvT _3v6Dr">
                                                  <div>
                                                    <label className="aLzYS _2P9Gi _6LXde">
                                                      <div className="_1YJhl">
                                                        <div className="_315kk">
                                                          <div className="_1h4cK">
                                                            <div>
                                                              <input
                                                                className="_3v6Dr _1nWNU"
                                                                autoComplete="vd-country-field"
                                                                data-id="vdCountryFieldnationality"
                                                                type="text"
                                                                defaultValue="Vietnam"
                                                              />
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </label>
                                                  </div>
                                                  <div />
                                                </div>
                                                <div className>
                                                  <div className="_2s99A _2D3rM" />
                                                  <div className="_2D3rM" />
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div>
                        <div className="zhgVB">
                          <div>
                            <div className="zhgVB">
                              <div className="css-1dbjc4n r-14lw9ot r-kdyh1x r-da5iq2 r-1udh08x">
                                <div
                                  className="css-1dbjc4n r-1awozwy r-qklmqi r-18u37iz r-ymttw5 r-1f1sjgu"
                                  style={{ borderBottomColor: 'rgb(205, 208, 209)' }}
                                >
                                  <div className="css-1dbjc4n r-1awozwy r-13awgt0 r-18u37iz r-88pszg">
                                    <img
                                      src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/dark/ic_bag_baggage_24px-54541a033f57db3f91866b0d4fdf518f.svg"
                                      width={24}
                                      height={24}
                                    />
                                    <h2
                                      aria-level={2}
                                      dir="auto"
                                      role="heading"
                                      className="css-4rbku5 css-901oao r-1sixt3s r-adyw6z r-b88u0q r-135wba7 r-1jkjb r-fdjqy7"
                                      style={{ color: 'rgb(3, 18, 26)' }}
                                    >
                                      Hành lý
                                    </h2>
                                  </div>
                                  <div
                                    aria-live="polite"
                                    role="button"
                                    tabIndex={0}
                                    className="css-18t94o4 css-1dbjc4n r-kdyh1x r-1loqt21 r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                  >
                                    <div
                                      className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                      style={{ opacity: 1 }}
                                    >
                                      <div
                                        dir="auto"
                                        className="css-901oao r-cygvgh r-b88u0q r-1iukymi r-q4m81j"
                                        style={{ color: 'rgb(1, 148, 243)' }}
                                      >
                                        Chọn hành lý
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="css-1dbjc4n"
                                  style={{ opacity: 1, transform: 'translate3d(0px, 0px, 0px)' }}
                                >
                                  <div className="css-1dbjc4n r-ymttw5 r-95jzfe r-a1fw50">
                                    <div
                                      className="css-1dbjc4n r-1awozwy r-18u37iz r-1h0z5md r-1ifxtd0 r-13qz1uu"
                                      data-testid="baggageSummaryRouteTitle-HAN-SGN"
                                    >
                                      <div
                                        dir="auto"
                                        className="css-901oao r-1sixt3s r-ubezar r-b88u0q r-135wba7 r-fdjqy7"
                                        style={{ color: 'rgb(3, 18, 26)' }}
                                      >
                                        Hà Nội (HAN)
                                      </div>
                                      <img
                                        src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/dark/ic_system_arrow_right_24px-a8fce838654c9c4fe1ca1fa42f770bde.svg"
                                        width={16}
                                        height={16}
                                        style={{ marginLeft: '4px', marginRight: '4px' }}
                                      />
                                      <div
                                        dir="auto"
                                        className="css-901oao r-1sixt3s r-ubezar r-b88u0q r-135wba7 r-fdjqy7"
                                        style={{ color: 'rgb(3, 18, 26)' }}
                                      >
                                        TP HCM (SGN)
                                      </div>
                                    </div>
                                    <div className="css-1dbjc4n r-1habvwh r-18u37iz r-1w6e6rj r-1h0z5md">
                                      <div className="css-1dbjc4n r-1ifxtd0 r-88pszg r-8dmqhk">
                                        <div
                                          dir="auto"
                                          className="css-901oao css-bfa6kz r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                          data-testid="baggageSummaryRouteTraveler-0.0.0-name"
                                          style={{ color: 'rgb(104, 113, 118)' }}
                                        >
                                          Bà fghfgh fghfgh
                                        </div>
                                        <div className="css-1dbjc4n" style={{ opacity: 1 }}>
                                          <div
                                            dir="auto"
                                            className="css-901oao css-cens5h r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-14gqq1x r-fdjqy7"
                                            data-testid="baggageSummaryRouteTraveler-0.0.0-selection"
                                            style={{ WebkitLineClamp: 2, color: 'rgb(3, 18, 26)' }}
                                          >
                                            0 kg - 0&nbsp;₫
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="zhgVB">
                          <h3 className="_2pVrN _2xTe7 _1ucNT">Tùy chọn bổ sung:</h3>
                          <div>
                            <div className="zhgVB">
                              <div className="mMmI2 _2JlHX _1ZzOp _2BklA" data-elevation={1}>
                                <div
                                  className="_1N3Xp _3VDAS"
                                  style={{
                                    backgroundImage:
                                      'url("https://ik.imagekit.io/tvlk/image/imageResource/2018/12/07/1544155519261-a8e02e0056a540f36dd7385d3c89621d.jpeg?tr=q-75")',
                                  }}
                                >
                                  <label className="_1iwut JDPpE" data-disabled="false">
                                    <input className="_1uJkS" type="checkbox" />
                                    <div>
                                      <div className="_2E633">
                                        <svg
                                          fill="#fff"
                                          stroke="#fff"
                                          className="_14AeJ"
                                          height={24}
                                          strokeLinecap="round"
                                          viewBox="0 0 24 24"
                                          width={24}
                                          xmlns="http://www.w3.org/2000/svg"
                                          xmlnsXlink="http://www.w3.org/1999/xlink"
                                        >
                                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                                        </svg>
                                      </div>
                                      <div className="xTT_j" />
                                    </div>
                                  </label>
                                  <div className="_3BSj2 d8OiG">
                                    <div className="_29CdC">
                                      <div className="d8OiG">
                                        <h4 className="_2rus_">Chubb Travel Protection</h4>
                                        <div>
                                          <span className="_1KrnW _1EnnQ _1UHMc">45.000 VND</span>
                                          <span> </span>
                                          <span className="_1KrnW _1EnnQ Edww1">/vé</span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="_3VDAS _3Um5g">
                                  <span className="_1KrnW _1EnnQ Edww1">
                                    Bảo hiểm lên đến 210 triệu VND cho Quyền lợi Tai nạn, lên đến
                                    toàn bộ giá trị vé máy bay cho Quyền lợi Hủy Chuyến Bay, lên đến
                                    2.6 triệu VND cho Quyền lợi Chuyến bay và Hành lý bị trì hoãn.
                                    Khi mua bảo hiểm du lịch, Quý khách đã đồng ý mọi điều khoản do
                                    Chubb quy định
                                    <br />
                                    Người được bảo hiểm từ 6 tuần tuổi đến 85 tuổi.
                                    <br />
                                    <b>THÔNG BÁO:</b> Hợp đồng bảo hiểm mua sau ngày 19th March 2020
                                    sẽ không được bảo hiểm cho các khoản bồi thường trực tiếp hoặc
                                    gián tiếp phát sinh từ, liên quan đến hoặc theo bất kỳ cách nào
                                    được kết nối với COVID-19 (hoặc bất kỳ đột biến hoặc biến thể,
                                    biến dạng có liên quan).
                                  </span>
                                </div>
                                <div className="_3VDAS">
                                  <a>
                                    <span className="_1KrnW jjGhl box20 _1dKIX">Chi tiết</span>
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="zhgVB">
                              <div className="mMmI2 _2JlHX _1ZzOp _2BklA" data-elevation={1}>
                                <div
                                  className="_1N3Xp _3VDAS"
                                  style={{
                                    backgroundImage:
                                      'url("https://ik.imagekit.io/tvlk/image/imageResource/2018/12/07/1544155519261-a8e02e0056a540f36dd7385d3c89621d.jpeg?tr=q-75")',
                                  }}
                                >
                                  <label className="_1iwut JDPpE" data-disabled="false">
                                    <input className="_1uJkS" type="checkbox" />
                                    <div>
                                      <div className="_2E633">
                                        <svg
                                          fill="#fff"
                                          stroke="#fff"
                                          className="_14AeJ"
                                          height={24}
                                          strokeLinecap="round"
                                          viewBox="0 0 24 24"
                                          width={24}
                                          xmlns="http://www.w3.org/2000/svg"
                                          xmlnsXlink="http://www.w3.org/1999/xlink"
                                        >
                                          <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
                                        </svg>
                                      </div>
                                      <div className="xTT_j" />
                                    </div>
                                  </label>
                                  <div className="_3BSj2 d8OiG">
                                    <div className="_29CdC">
                                      <div className="d8OiG">
                                        <h4 className="_2rus_">
                                          Bảo Hiểm Chubb Baggage Protection
                                        </h4>
                                        <div>
                                          <span className="_1KrnW _1EnnQ _1UHMc">16.000 VND</span>
                                          <span> </span>
                                          <span className="_1KrnW _1EnnQ Edww1">/vé</span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="_3VDAS _3Um5g">
                                  <span className="_1KrnW _1EnnQ Edww1">
                                    Bảo hiểm lên đến VND 20,000,000 từ Bảo hiểm Chubb Travel Protect
                                    - Quyền lợi Hành lý và vật dụng cá nhân mang theo do Mất hoặc hư
                                    hại đối với hành lý, quần áo và các vật dụng cá nhân. Với việc
                                    lựa chọn chương trình Bảo hiểm này, Quý Khách đã hiểu và đồng ý
                                    với Quyền lợi, Quy tắc bảo hiểm và Nội dung tuyên bố và ủy quyền
                                    do Công ty TNHH Bảo hiểm Chubb Việt Nam quy định.
                                  </span>
                                </div>
                                <div className="_3VDAS">
                                  <a>
                                    <span className="_1KrnW jjGhl box20 _1dKIX">Chi tiết</span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="zhgVB">
                          <div>
                            <div>
                              <h3 className="_2pVrN _2xTe7">Chi tiết giá</h3>
                              <div className="mMmI2 _3GueF" data-elevation={1}>
                                <div className>
                                  <div className="_2PNzA">
                                    <div className="_3HMVE _1ZzOp">
                                      <div className="_35ECe">
                                        <div className="wu8bG">
                                          <h4 className="_2rus_">Giá bạn trả</h4>
                                        </div>
                                        <div className="_3Wwff">554.900 VND</div>
                                        <svg
                                          strokeWidth="1.5"
                                          viewBox="0 0 16 16"
                                          className="_2vdng"
                                          fill="none"
                                          height={24}
                                          stroke="currentColor"
                                          strokeLinecap="round"
                                          width={24}
                                          xmlns="http://www.w3.org/2000/svg"
                                          xmlnsXlink="http://www.w3.org/1999/xlink"
                                        >
                                          <g transform="translate(0.166667, 4.033325)">
                                            <path d="M7.33333333,7.33333333 L0.488888889,0.488888889" />
                                            <path d="M7.33333333,7.33333333 L14.1777778,0.488888889" />
                                          </g>
                                        </svg>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="_2eCs0" />
                                </div>
                                <div className="_1pDLB">
                                  <div>
                                    <div
                                      className="_3Q-sa lLNFQ"
                                      style={{ backgroundColor: 'rgb(244, 251, 254)' }}
                                    >
                                      <div className="_28Oaq mMcrn">
                                        <div className="mMcrn _1E1LD _2Opg_">
                                          <svg
                                            viewBox="0 0 24 24"
                                            fill="#0770cd"
                                            height={24}
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            width={24}
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlnsXlink="http://www.w3.org/1999/xlink"
                                          >
                                            <path
                                              fill="#0770cd"
                                              strokeWidth={0}
                                              fillRule="evenodd"
                                              d="M12,24 C5.372583,24 0,18.627417 0,12 C0,5.372583 5.372583,0 12,0 C18.627417,0 24,5.372583 24,12 C24,18.627417 18.627417,24 12,24 Z M10.5,11.4975446 L10.5,18.5024554 C10.5,19.0536886 10.9472481,19.5 11.4989566,19.5 L12.5010434,19.5 C13.0573397,19.5 13.5,19.053384 13.5,18.5024554 L13.5,11.4975446 C13.5,10.9463114 13.0527519,10.5 12.5010434,10.5 L11.4989566,10.5 C10.9426603,10.5 10.5,10.946616 10.5,11.4975446 Z M10.5,6.99895656 L10.5,8.00104344 C10.5,8.55733967 10.9472481,9 11.4989566,9 L12.5010434,9 C13.0573397,9 13.5,8.55275191 13.5,8.00104344 L13.5,6.99895656 C13.5,6.44266033 13.0527519,6 12.5010434,6 L11.4989566,6 C10.9426603,6 10.5,6.44724809 10.5,6.99895656 Z"
                                            />
                                          </svg>
                                        </div>
                                        <div className="_36qZA _1q_BZ">
                                          <span className="_1KrnW jjGhl Edww1">
                                            Đăng nhập hoặc Đăng ký để tích ngay điểm thưởng! Bạn có
                                            thể tiếp tục đặt chỗ sau vì tiến trình đã được lưu trên
                                            hệ thống.
                                          </span>
                                        </div>
                                      </div>
                                      <div className="_3J_jq">
                                        <a target="_self">
                                          <span className="_1KrnW jjGhl _2HSse">Đăng nhập</span>
                                        </a>
                                      </div>
                                    </div>
                                    <div>
                                      <div className="zYvfE">
                                        <div className="VJPQJ">
                                          <div className="_2_D95">
                                            <div className="_3sM-4">
                                              <div className="_2AKT2 _1FU8b">
                                                <div>
                                                  <div className="_2Qki3 _3dM-n">
                                                    <svg
                                                      viewBox="0 0 25 25"
                                                      fill="#434343"
                                                      height={24}
                                                      stroke="currentColor"
                                                      strokeLinecap="round"
                                                      width={24}
                                                      xmlns="http://www.w3.org/2000/svg"
                                                      xmlnsXlink="http://www.w3.org/1999/xlink"
                                                    >
                                                      <g transform="translate(1.000000, 1.000000)">
                                                        <g
                                                          stroke="none"
                                                          strokeWidth={1}
                                                          fill="none"
                                                          fillRule="evenodd"
                                                        >
                                                          <g fill="#434343">
                                                            <g>
                                                              <path d="M12,13.4142136 L5.70710678,19.7071068 C5.31658249,20.0976311 4.68341751,20.0976311 4.29289322,19.7071068 C3.90236893,19.3165825 3.90236893,18.6834175 4.29289322,18.2928932 L10.5893105,12.003524 L4.29289322,5.70710678 C3.90236893,5.31658249 3.90236893,4.68341751 4.29289322,4.29289322 C4.68341751,3.90236893 5.31658249,3.90236893 5.70710678,4.29289322 L12,10.5857864 L18.2928932,4.29289322 C18.6834175,3.90236893 19.3165825,3.90236893 19.7071068,4.29289322 C20.0976311,4.68341751 20.0976311,5.31658249 19.7071068,5.70710678 L13.4180748,11.9961388 L19.7071068,18.2928932 C20.0976311,18.6834175 20.0976311,19.3165825 19.7071068,19.7071068 C19.3165825,20.0976311 18.6834175,20.0976311 18.2928932,19.7071068 L12,13.4142136 L12,13.4142136 Z" />
                                                            </g>
                                                          </g>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                  </div>
                                                  <div>
                                                    <div className="_29hyg">
                                                      <div>
                                                        <div>
                                                          <div className="lyY7- _3llKY">
                                                            <div />
                                                            <div className="register-pop--top">
                                                              <h1>Đăng nhập tài khoản</h1>
                                                              <div className="_2h7qe">
                                                                <div className>
                                                                  <label className="GmGpq">
                                                                    Email hoặc số di động
                                                                  </label>
                                                                  <input
                                                                    type="text"
                                                                    name="username"
                                                                    data-id="usernameField"
                                                                  />
                                                                  <div className>
                                                                    <div className="_2s99A _2D3rM" />
                                                                    <div className="_2D3rM" />
                                                                  </div>
                                                                </div>
                                                                <br />
                                                                <div className>
                                                                  <label className="GmGpq">
                                                                    Mật khẩu
                                                                  </label>
                                                                  <input
                                                                    type="password"
                                                                    name="password"
                                                                    data-id="passwordField"
                                                                  />
                                                                  <div className>
                                                                    <div className="_2s99A _2D3rM" />
                                                                    <div className="_2D3rM" />
                                                                  </div>
                                                                </div>
                                                                <div className="E8rMA">
                                                                  <a
                                                                    href="https://www.traveloka.com/vi-vn/forgotpassword"
                                                                    target="_blank"
                                                                    id="linkForgot"
                                                                  >
                                                                    Quên mật khẩu
                                                                  </a>
                                                                </div>
                                                              </div>
                                                              <div>
                                                                <button
                                                                  className="_3n-wd _3_ByF gLbQ- _1AdOq _90_75"
                                                                  type="button"
                                                                  data-id="submitBtn"
                                                                >
                                                                  Đăng Nhập
                                                                </button>
                                                              </div>
                                                              <p className="_1OKZT">
                                                                <span>Bạn chưa có tài khoản? </span>
                                                                <a href="#">Đăng ký</a>
                                                              </p>
                                                            </div>
                                                            <div />
                                                          </div>
                                                          <div className="_1nv7Z">
                                                            <div className="register-pop--social">
                                                              <span>
                                                                <div
                                                                  className="mMmI2 E32Rd _2gTsp"
                                                                  data-elevation={1}
                                                                >
                                                                  <div className="_2XtJs">
                                                                    <svg
                                                                      strokeWidth={0}
                                                                      fill="none"
                                                                      height={24}
                                                                      stroke="currentColor"
                                                                      strokeLinecap="round"
                                                                      viewBox="0 0 24 24"
                                                                      width={24}
                                                                      xmlns="http://www.w3.org/2000/svg"
                                                                      xmlnsXlink="http://www.w3.org/1999/xlink"
                                                                      style={{
                                                                        background:
                                                                          'rgb(59, 89, 152)',
                                                                      }}
                                                                    >
                                                                      <path
                                                                        fill="#fff"
                                                                        d="M7,8.60454545 L9.35682098,8.60454545 L9.35682098,6.29545455 C9.35682098,5.27727273 9.38385907,3.70454545 10.1138878,2.73181818 C10.8889799,1.7 11.9479721,1 13.7730438,1 C16.7472347,1 18,1.42727273 18,1.42727273 L17.4096682,4.95 C17.4096682,4.95 16.4272839,4.66363636 15.5124949,4.66363636 C14.5977059,4.66363636 13.7730438,4.99545455 13.7730438,5.91818182 L13.7730438,8.60454545 L17.5313396,8.60454545 L17.2744777,12.0454545 L13.7775502,12.0454545 L13.7775502,24 L9.35682098,24 L9.35682098,12.0454545 L7,12.0454545 L7,8.60454545 Z"
                                                                      />
                                                                    </svg>
                                                                  </div>
                                                                  <div className="vi0ep Qr5W9">
                                                                    Liên kết với Facebook
                                                                  </div>
                                                                </div>
                                                                <div
                                                                  className="mMmI2 E32Rd _3m4bQ"
                                                                  data-elevation={1}
                                                                >
                                                                  <div className="_2XtJs">
                                                                    <svg
                                                                      viewBox="0 0 50 50"
                                                                      strokeWidth={0}
                                                                      fill="#FFFFFF"
                                                                      stroke="none"
                                                                      height={24}
                                                                      strokeLinecap="round"
                                                                      width={24}
                                                                      xmlns="http://www.w3.org/2000/svg"
                                                                      xmlnsXlink="http://www.w3.org/1999/xlink"
                                                                    >
                                                                      <path
                                                                        d="M45,1H5C2.8,1,1,2.8,1,5v40c0,2.2,1.8,4,4,4h40c2.2,0,4-1.8,4-4V5C49,2.8,47.2,1,45,1z"
                                                                        fill="#FFFFFF"
                                                                      />
                                                                      <g>
                                                                        <g>
                                                                          <path
                                                                            d="M20.3,10.5c3.3-1.1,7-1.1,10.3,0.1c1.8,0.7,3.5,1.7,4.9,3.1c-0.5,0.5-1,1-1.5,1.5    c-0.9,0.9-1.9,1.9-2.8,2.8c-0.9-0.9-2.1-1.6-3.3-1.9c-1.4-0.4-3-0.5-4.5-0.2c-1.7,0.4-3.3,1.3-4.6,2.5c-1,1-1.8,2.3-2.2,3.6    c-1.7-1.3-3.3-2.6-5-3.9C13.4,14.6,16.6,11.8,20.3,10.5z"
                                                                            fill="#EA4335"
                                                                          />
                                                                        </g>
                                                                        <g>
                                                                          <path
                                                                            d="M10.3,22c0.3-1.3,0.7-2.6,1.3-3.9c1.7,1.3,3.3,2.6,5,3.9c-0.7,1.9-0.7,4,0,6c-1.7,1.3-3.3,2.6-5,3.9    C10.1,28.8,9.6,25.3,10.3,22z"
                                                                            fill="#FBBC05"
                                                                          />
                                                                        </g>
                                                                        <g>
                                                                          <path
                                                                            d="M25.3,22.1c4.8,0,9.6,0,14.4,0c0.5,2.7,0.4,5.5-0.4,8.1c-0.7,2.4-2,4.6-3.9,6.4c-1.6-1.3-3.2-2.5-4.9-3.8    c1.6-1.1,2.7-2.8,3.1-4.7c-2.8,0-5.6,0-8.3,0C25.3,26.1,25.3,24.1,25.3,22.1z"
                                                                            fill="#4285F4"
                                                                          />
                                                                        </g>
                                                                        <g>
                                                                          <path
                                                                            d="M11.6,31.9c1.7-1.3,3.3-2.6,5-3.9c0.6,1.9,1.9,3.6,3.5,4.7c1,0.7,2.2,1.2,3.4,1.5c1.2,0.2,2.4,0.2,3.7,0    c1.2-0.2,2.4-0.7,3.4-1.3c1.6,1.3,3.2,2.5,4.9,3.8c-1.8,1.6-3.9,2.7-6.3,3.3c-2.6,0.6-5.3,0.6-7.8-0.1c-2-0.5-3.9-1.5-5.6-2.8    C14.1,35.6,12.6,33.8,11.6,31.9z"
                                                                            fill="#34A853"
                                                                          />
                                                                        </g>
                                                                      </g>
                                                                    </svg>
                                                                  </div>
                                                                  <div className="vi0ep">
                                                                    Liên kết với Google
                                                                  </div>
                                                                </div>
                                                                <div />
                                                              </span>
                                                            </div>
                                                          </div>
                                                          <div>
                                                            <div className="zYvfE">
                                                              <div className="VJPQJ">
                                                                <div className="_2_D95">
                                                                  <div className="_3sM-4">
                                                                    <div className="_2AKT2">
                                                                      <div>
                                                                        <div className="_2Qki3">
                                                                          <svg
                                                                            viewBox="0 0 25 25"
                                                                            fill="#434343"
                                                                            height={24}
                                                                            stroke="currentColor"
                                                                            strokeLinecap="round"
                                                                            width={24}
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            xmlnsXlink="http://www.w3.org/1999/xlink"
                                                                          >
                                                                            <g transform="translate(1.000000, 1.000000)">
                                                                              <g
                                                                                stroke="none"
                                                                                strokeWidth={1}
                                                                                fill="none"
                                                                                fillRule="evenodd"
                                                                              >
                                                                                <g fill="#434343">
                                                                                  <g>
                                                                                    <path d="M12,13.4142136 L5.70710678,19.7071068 C5.31658249,20.0976311 4.68341751,20.0976311 4.29289322,19.7071068 C3.90236893,19.3165825 3.90236893,18.6834175 4.29289322,18.2928932 L10.5893105,12.003524 L4.29289322,5.70710678 C3.90236893,5.31658249 3.90236893,4.68341751 4.29289322,4.29289322 C4.68341751,3.90236893 5.31658249,3.90236893 5.70710678,4.29289322 L12,10.5857864 L18.2928932,4.29289322 C18.6834175,3.90236893 19.3165825,3.90236893 19.7071068,4.29289322 C20.0976311,4.68341751 20.0976311,5.31658249 19.7071068,5.70710678 L13.4180748,11.9961388 L19.7071068,18.2928932 C20.0976311,18.6834175 20.0976311,19.3165825 19.7071068,19.7071068 C19.3165825,20.0976311 18.6834175,20.0976311 18.2928932,19.7071068 L12,13.4142136 L12,13.4142136 Z" />
                                                                                  </g>
                                                                                </g>
                                                                              </g>
                                                                            </g>
                                                                          </svg>
                                                                        </div>
                                                                        <div>
                                                                          <div className="_29hyg">
                                                                            <div className="tvat-user-otp-recipients-container _21-yY">
                                                                              <h2 className="_2vvS_">
                                                                                Yêu cầu xác thực
                                                                              </h2>
                                                                              <p>
                                                                                Chúng tôi nhận thấy
                                                                                bạn đang sử dụng
                                                                                thiết bị mới. Do đó,
                                                                                bạn cần phải xác
                                                                                thực tài khoản. Vui
                                                                                lòng chọn hình thức
                                                                                nhận mã xác thực
                                                                                qua:
                                                                              </p>
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                    <div className="_21o08">
                                                                      <div className="_1Koj6 l9JwO">
                                                                        1
                                                                      </div>
                                                                      <div className="l9JwO">2</div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <div />
                                                          </div>
                                                        </div>
                                                        <div />
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="zhgVB">
                          <div>
                            <div>
                              <div
                                className="css-1dbjc4n r-kdyh1x r-xyw6el"
                                style={{
                                  backgroundColor: 'rgb(255, 250, 217)',
                                  borderColor: 'rgb(217, 152, 0)',
                                  borderWidth: '1px',
                                }}
                              >
                                <div className="css-1dbjc4n r-18u37iz">
                                  <div className="css-1dbjc4n r-1kb76zh">
                                    <img
                                      src="https://d1785e74lyxkqq.cloudfront.net/godwit/lib/_/_/node_modules/@traveloka/icon-kit-web/svg/yellowPrimary/ic_system_status_warning-fill_24px-d8a0587d0b172eedd46f4b01f2ef0de9.svg"
                                      width={16}
                                      height={16}
                                    />
                                  </div>
                                </div>
                                <div className="css-1dbjc4n r-14gqq1x">
                                  <div
                                    dir="auto"
                                    className="css-901oao r-1sixt3s r-1b43r93 r-majxgm r-rjixqe r-fdjqy7"
                                    style={{ color: 'rgb(177, 84, 0)' }}
                                  >
                                    Trước khi đặt vé, hãy đảm bảo rằng bạn có thể đáp ứng tất cả các
                                    yêu cầu đi lại theo quy định của cơ quan quản lý khu vực và quốc
                                    gia. Kiểm tra các yêu cầu đầy đủ tại
                                    https://trv.lk/travel-regulation hoặc nhấn vào đường link bên
                                    dưới.
                                  </div>
                                </div>
                                <div
                                  aria-live="polite"
                                  role="button"
                                  tabIndex={0}
                                  className="css-18t94o4 css-1dbjc4n r-1habvwh r-kdyh1x r-1loqt21 r-1h0z5md r-14gqq1x r-10paoce r-5njf8e r-1otgn73 r-lrvibr"
                                  style={{}}
                                >
                                  <div
                                    className="css-1dbjc4n r-1awozwy r-18u37iz r-1777fci"
                                    style={{ opacity: 1 }}
                                  >
                                    <div
                                      dir="auto"
                                      className="css-901oao r-1o4mh9l r-b88u0q r-f0eezp r-q4m81j"
                                      style={{ color: 'rgb(1, 148, 243)' }}
                                    >
                                      Chi tiết yêu cầu
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="_1m0YZ">
                          <button
                            className=" _9Srcv gLbQ- _90_75"
                            type="button"
                            data-id="stdzBookingContinueToPayment"
                          >
                            Tiếp tục
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="_1Jyop">
                  <div className="zhgVB">
                    <div className="zhgVB">
                      <div className="zhgVB">
                        <div style={{ marginBottom: '16px' }}>
                          <div className="mMmI2 _2JlHX" data-elevation={1}>
                            <div className="_3HMVE nNMc_">
                              <svg
                                strokeWidth={0}
                                fill="#30C5F7"
                                height={24}
                                stroke="currentColor"
                                strokeLinecap="round"
                                viewBox="0 0 24 24"
                                width={24}
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                              >
                                <g>
                                  <path d="M11.964558,6.14125661 L13.8541043,6.51916587 L18.4496899,2.52012268 C19.2945553,1.78492726 20.5835088,1.83683661 21.3736438,2.62697164 C22.1650971,3.41842491 22.2129336,4.70922562 21.4804928,5.55092556 L17.4814496,10.1465111 L17.8594278,12.0364023 C18.2650613,11.7637492 18.8153146,11.8028394 19.1734819,12.1610067 C19.5785205,12.5660453 19.5817196,13.2195436 19.1666108,13.6346524 L18.3436667,14.4575965 L19.4571198,20.0248621 C19.6924132,21.2013294 18.0749946,21.7715929 17.5203924,20.7077062 L13.7517141,13.4782961 L11.0209323,15.9043041 C10.8211477,16.0642187 10.5379244,16.2308982 10.1689883,16.3363433 C10.0940337,16.357766 10.0187022,16.3753374 9.94322578,16.3889171 L9.94322578,20.9616545 C9.94322578,22.0135724 8.55790407,22.3972843 8.01669675,21.4952721 L5.94993922,18.0506762 L2.50534335,15.9839187 C1.60333115,15.4427114 1.98704305,14.0573897 3.038961,14.0573897 L7.61169837,14.0573897 C7.62527807,13.9819133 7.64284943,13.9065817 7.66427211,13.8316272 C7.76971729,13.4626911 7.93639679,13.1794677 8.1308429,12.9387544 L10.521982,10.2487255 L3.29290925,6.48022306 C2.22902259,5.92562089 2.79928603,4.30820221 3.97575336,4.54349567 L9.54301892,5.65694879 L10.3659631,4.83400461 C10.7747965,4.42517121 11.4317463,4.41927109 11.8396087,4.82713358 C12.195861,5.18338587 12.241246,5.7318492 11.964558,6.14125661 L11.964558,6.14125661 Z" />
                                </g>
                              </svg>
                              <h5 title="Hà Nội → TP HCM" className="_34Lr4 hkbF8">
                                Hà Nội → TP HCM
                              </h5>
                              <span className="_1KrnW _1EnnQ box20 _1dKIX _2HfuK">Chi tiết</span>
                            </div>
                            <hr className="_2si0n" data-inset="false" />
                            <div className="ji-c9 _1E1LD _1kbWY">
                              <h5 className="_34Lr4">Chuyến bay đi • Wed, 09 Jun 2021</h5>
                              <div>
                                <div>
                                  <div className="_3AkXH">
                                    <div className="_1LiWW">
                                      <div className="_3f7oj">
                                        <span className="_1KrnW _1EnnQ _2HSse _1dKIX">
                                          VietJet Air
                                        </span>
                                      </div>
                                      <div>
                                        <span className="_1KrnW _1EnnQ Edww1">Khuyến mãi</span>
                                      </div>
                                    </div>
                                    <div>
                                      <img
                                        className="cADpK"
                                        src="https://ik.imagekit.io/tvlk/image/imageResource/2015/12/17/1450350670702-a9dba44d3e9fe096f83ffe00d56a99ec.png?tr=q-75"
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className="_10ZkQ">
                                    <div className="OMkfM fNyzk">
                                      <div>
                                        <div>
                                          <span className="_1KrnW _1EnnQ _2HSse">12:20</span>
                                        </div>
                                        <div className="n4VYU"> HAN </div>
                                      </div>
                                      <div className="_1m9Ud">
                                        <svg
                                          strokeWidth={0}
                                          width={12}
                                          height={12}
                                          viewBox="0 0 12 12"
                                          fill="#8f8f8f"
                                          stroke="currentColor"
                                          strokeLinecap="round"
                                          xmlns="http://www.w3.org/2000/svg"
                                          xmlnsXlink="http://www.w3.org/1999/xlink"
                                        >
                                          <path
                                            fill="#8f8f8f"
                                            fillRule="evenodd"
                                            d="M8.58578644,5 L1.99539757,5 C1.4556644,5 1,5.44771525 1,6 C1,6.55613518 1.44565467,7 1.99539757,7 L8.58578644,7 L7.29289322,8.29289322 C6.90236893,8.68341751 6.90236893,9.31658249 7.29289322,9.70710678 C7.68341751,10.0976311 8.31658249,10.0976311 8.70710678,9.70710678 L11.7071068,6.70710678 C12.0976311,6.31658249 12.0976311,5.68341751 11.7071068,5.29289322 L8.70710678,2.29289322 C8.31658249,1.90236893 7.68341751,1.90236893 7.29289322,2.29289322 C6.90236893,2.68341751 6.90236893,3.31658249 7.29289322,3.70710678 L8.58578644,5 L8.58578644,5 Z"
                                          />
                                        </svg>
                                      </div>
                                      <div>
                                        <div>
                                          <span className="_1KrnW _1EnnQ _2HSse">14:30</span>
                                        </div>
                                        <div className="n4VYU"> SGN </div>
                                      </div>
                                      <div>
                                        <div>
                                          <span className="_1KrnW _1EnnQ _2HSse">2h 10m</span>
                                        </div>
                                        <span className="_1KrnW _1EnnQ Edww1">
                                          <span className="_3Hkm2 _8Yf_q" />
                                          Bay thẳng
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="tBHqZ" />
                              </div>
                            </div>
                            <div className="_3VDAS">
                              <span className="_1KrnW _1EnnQ Edww1 _22csp _2nFkQ">
                                <svg
                                  strokeWidth={0}
                                  width={16}
                                  height={16}
                                  viewBox="0 0 16 16"
                                  className="_3GhTG"
                                  fill="none"
                                  stroke="currentColor"
                                  strokeLinecap="round"
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                >
                                  <g fill="none" fillRule="evenodd" strokeWidth={0}>
                                    <rect width={16} height={16} />
                                    <circle cx={8} cy={8} r={7} strokeWidth={2} />
                                    <path
                                      stroke="currentColor"
                                      strokeLinecap="round"
                                      strokeWidth={2}
                                      d="M8,8 L8,12"
                                    />
                                    <circle cx={8} cy={5} r={1} fill="currentColor" />
                                  </g>
                                </svg>
                                <span>Không hoàn tiền</span>
                              </span>
                              <span className="_1KrnW _1EnnQ KVDS- _22csp _2nFkQ">
                                <svg
                                  strokeWidth={0}
                                  width={16}
                                  height={16}
                                  viewBox="0 0 16 16"
                                  className="_3GhTG"
                                  fill="none"
                                  stroke="currentColor"
                                  strokeLinecap="round"
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                >
                                  <g fill="none" fillRule="evenodd">
                                    <path d="M0 0h16v16H0z" />
                                    <circle strokeWidth={2} cx={8} cy={8} r={7} />
                                    <path
                                      strokeWidth={2}
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M5 8l2 2 4-4"
                                    />
                                  </g>
                                </svg>
                                <span>Có áp dụng đổi lịch bay</span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
