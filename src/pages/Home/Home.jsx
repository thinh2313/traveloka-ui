import React, { Fragment } from 'react';
import './Home.css';

export const Home = () => {
  return (
    <Fragment>
      <div className="LbQ-i _3yiVT">
        <div style={{ width: '960px' }} className="mMmI2 _2vjgs" data-elevation="2">
          <div></div>
          <div className="_1uz2h">
            <div className="yYXXL">
              <div className="_3j6uD">
                <div className="_3h9L8">
                  <div
                    className="_1SnbR tvat-FLIGHT tvat-search-box-flight"
                    className
                  >
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#30C5F7',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Vé máy bay</span>
                    </div>
                  </div>
                  <div
                    className="_1SnbR sL9dg tvat-HOTEL tvat-search-box-hotel"
                    style={{ borderColor: '#235D9F' }}
                  >
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#235D9F',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Khách sạn</span>
                    </div>
                  </div>
                  <div
                    className="_1SnbR tvat-PACKAGE tvat-search-box-flight-hotel"
                  >
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#931682',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Combo tiết kiệm</span>
                    </div>
                  </div>
                  <div
                    className="_1SnbR tvat-AIRPORT_TRANSFER tvat-search-box-airport-transport"
                  >
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#6DD3CE',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Đưa đón sân bay</span>
                    </div>
                  </div>
                  <div
                    className="_1SnbR tvat-EXPERIENCE tvat-search-box-experience"
                  >
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#FF6D70',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Xperience</span>
                    </div>
                  </div>
                  <div className="_1SnbR tvat-VEHICLE_RENTAL">
                    <div>
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: '#087E8B',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">Cho thuê xe</span>
                    </div>
                  </div>
                  <div
                    className="_1SnbR tvat-TRAIN_GLOBAL tvat-search-box-global-train"
                    className
                  >
                    <div data-id="train-global-search">
                      <span
                        className="svg-icons"
                        style={{
                          width: '16px',
                          height: '16px',
                          backgroundColor: 'none',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="wfhct">JR Pass</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="_9J40S">
              <div className="Qbd3p">
                <div className="csWKl">
                  <span
                    className="svg-icons"
                    style={{
                      width: '8px',
                      height: '20px',
                      backgroundColor: 'none',
                      opacity: '0.3',
                      display: 'inline-block',
                      borderRadius: '25%',
                    }}
                  ></span>
                </div>
              </div>
            </div>
            <div className="_3Q_9s">
              <div className="tvat-search-box-hotel">
                <div className="mMmI2 _3T7AX" data-elevation="0">
                  <div className="_19yPk">
                    <a
                      href="https://www.traveloka.com/vi-vn/hotel/last-view"
                      rel="nofollow"
                      className="_1r_fg"
                    >
                      <span
                        className="svg-icons _2tk-r"
                        style={{
                          width: '24px',
                          height: '24px',
                          backgroundColor: 'none',
                          opacity: '0.3',
                          display: 'inline-block',
                          borderRadius: '25%',
                        }}
                      ></span>
                      <span className="_2m1JC">Khách sạn xem gần đây</span>
                    </a>
                  </div>
                  <div className="_2U03q">
                    <div className="_1MlOC _1nvUc">
                      <div className="_13--o _1nKD-">
                        <div className="_1IGGD">
                          <div className="_39fQR ivxVS _2lEn7 _3MAbS">
                            <label>Thành phố, địa điểm hoặc tên khách sạn:</label>
                            <label className="lKyxz"></label>
                            <input
                              type="text"
                              className="_1cRq1"
                              value="Yo"
                              data-id="hotelDestination"
                              autoComplete="hotel-autocomplete"
                              placeholder="Thành phố, khách sạn, điểm đến"
                              onChange={() => { }}
                            />
                            <div className="_3Hpw-">
                              <span
                                className="svg-icons"
                                style={{
                                  width: '24px',
                                  height: '24px',
                                  backgroundColor: '#8F8F8F',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                          </div>
                          <div></div>
                        </div>
                      </div>
                    </div>
                    <div className="_1MlOC _1nvUc">
                      <div className="_1bH2V _1nKD-">
                        <div className="_2PgYO inputDatePicker">
                          <div className="_39fQR ivxVS _2lEn7 _3MAbS">
                            <label>Nhận phòng:</label>
                            <label className="lKyxz"></label>
                            <input
                              type="text"
                              className="_1cRq1"
                              value="Yo"
                              data-id="checkInDate"
                              readOnly
                              placeholder="dd/mm/yyyy"
                              onChange={() => { }}
                            />
                            <div className="_3Hpw-">
                              <span
                                className="svg-icons"
                                style={{
                                  width: '24px',
                                  height: '24px',
                                  backgroundColor: '#8F8F8F',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                          </div>
                          <div></div>
                        </div>
                      </div>
                      <div className="_1bH2V _1nKD-">
                        <div className="_1IGGD">
                          <div className="_39fQR ivxVS _2lEn7 _3MAbS mtJcw">
                            <label>Số đêm:</label>
                            <label className="lKyxz"></label>
                            <input
                              type="text"
                              className="_1cRq1"
                              value="Yo"
                              data-id="durationOfNights"
                              placeholder="Đêm"
                              onChange={() => { }}
                            />
                            <div className="_3Hpw-">
                              <span
                                className="svg-icons"
                                style={{
                                  width: '24px',
                                  height: '24px',
                                  backgroundColor: '#8F8F8F',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                            <div className="VEXk0 Ktrf9">
                              <span
                                className="svg-icons"
                                style={{
                                  width: '14px',
                                  height: '14px',
                                  backgroundColor: 'none',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                          </div>
                          <div></div>
                        </div>
                      </div>
                      <div className="_3oLi_">
                        <div className="TvtE4">
                          <div className="_39fQR _ONE8 _2lEn7">
                            <label>Trả phòng:</label>
                            <label className="lKyxz"></label>
                            <input
                              type="text"
                              className="_1cRq1 QPhmr"
                              value="Yo"
                              data-id="checkOutDate"
                              placeholder="dd/mm/yyyy"
                              onChange={() => { }}
                            />
                          </div>
                          <div></div>
                        </div>
                      </div>
                    </div>
                    <div className="_1MlOC _1nvUc ZJooc">
                      <div className="_3S5NV _1nKD- _3SpBl">
                        <div className="tJfD_">
                          <div className="_39fQR ivxVS _2lEn7 _3MAbS mtJcw">
                            <label>Khách và Phòng</label>
                            <label className="lKyxz"></label>
                            <input
                              type="text"
                              className="_1cRq1"
                              value="Yo"
                              readOnly
                              placeholder="Khách, Phòng"
                              onChange={() => { }}
                            />
                            <div className="_3Hpw-">
                              <span
                                className="svg-icons twlOM"
                                style={{
                                  width: '24px',
                                  height: '24px',
                                  backgroundColor: '#8F8F8F',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                            <div className="VEXk0 Ktrf9">
                              <span
                                className="svg-icons"
                                style={{
                                  width: '14px',
                                  height: '14px',
                                  backgroundColor: 'none',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                          </div>
                          <div></div>
                        </div>
                      </div>
                      <div className="_1bH2V _1nKD- _3SpBl">
                        <button
                          className="_2cDEl tvat-hotelSubmitButton _22K0g gLbQ- _90_75"
                          type="button"
                        >
                          <div className="afC62 _3pjEu">
                            <span
                              className="svg-icons"
                              style={{
                                width: '24px',
                                height: '24px',
                                backgroundColor: '#fff',
                                opacity: '0.3',
                                display: 'inline-block',
                                borderRadius: '25%',
                              }}
                            ></span>
                          </div>
                          Tìm khách sạn
                        </button>
                      </div>
                    </div>
                    <div className="_1MlOC _1nvUc">
                      <div className="_13--o _1nKD- _3SpBl _10VAw">
                        <label className="_1iwut" data-disabled="false">
                          <input type="checkbox" className="_1uJkS" defaultChecked />
                          <div>
                            <div className="_2E633">
                              <span
                                className="svg-icons _14AeJ"
                                style={{
                                  width: '24px',
                                  height: '24px',
                                  backgroundColor: '#fff',
                                  opacity: '0.3',
                                  display: 'inline-block',
                                  borderRadius: '25%',
                                }}
                              ></span>
                            </div>
                            <div className="xTT_j"></div>
                          </div>
                        </label>
                        <span className="J6qZ8">Tôi đi công tác</span>
                        <div className="_3rSR1 _3SpBl">
                          <div className="gr56-">
                            <img
                              src="https://d1785e74lyxkqq.cloudfront.net/next-asset/_next/static/images/ic_system_help-fill_24px-1aJRr.svg"
                              style={{ width: '17px', height: '17px' }}
                            />
                          </div>
                          <div className="_3Jjgz _3c1wD">
                            Khi bạn chọn ô này, chúng tôi sẽ hiển thị chỗ ở phù hợp cho chuyến công
                            tác của bạn.
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="_1MlOC _1nvUc">
                      <div className="_3SpBl _203WR _3S5NV _1nKD-">
                        <img
                          src="https://d1785e74lyxkqq.cloudfront.net/next-asset/_next/static/images/ic_hotel_pay_upon_check_in_24px-2jj6n.svg"
                          style={{ width: '24px', height: '24px' }}
                        />
                        <span className="_3mXy4">Thanh toán khi nhận phòng</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div></div>
        </div>
        <div></div>
      </div>
      <div>

      </div>
    </Fragment>
  );
};
