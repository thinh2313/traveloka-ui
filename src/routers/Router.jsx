import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { Contact, Home ,Booking,Confirm } from '../pages';
import { PublicRoute } from './PublicRoute';
import { Header, Footer, HeaderBooking } from '../components/common';
import { LayoutFull } from '../layout/LayoutFull';

export const Routers = () => {
  return (
    <Router>
      <Switch>
        <PublicRoute
          exact
          path="/"
          component={Home}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={Header}
          footer={Footer}
        />
        <PublicRoute
          exact
          path="/contact"
          component={Contact}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={Header}
          footer={Footer}
        />
        <PublicRoute
          exact
          path="/booking"
          component={Booking}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={HeaderBooking}
          footer={Footer}
        />
        <PublicRoute
          exact
          path="/confirm"
          component={Confirm}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={HeaderBooking}
          footer={Footer}
        />
      </Switch>
    </Router>
  );
};
