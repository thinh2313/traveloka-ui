import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers/root';

export const store = createStore(rootReducer, composeWithDevTools(compose(applyMiddleware(thunk))));
