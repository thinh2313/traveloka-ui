import myAxios from '../../api/axios';
import {
  GET_ALL_HOTELS_FAILURE,
  GET_ALL_HOTELS_REQUEST,
  GET_ALL_HOTELS_SUCCESS,
} from '../constants/hotel';

export const getAllHotelsRequest = () => {
  return (dispatch) => {
    dispatch({
      type: GET_ALL_HOTELS_REQUEST,
    });
    myAxios
      .get('//url')
      .then((res) => {
        dispatch({
          type: GET_ALL_HOTELS_SUCCESS,
          payload: res.data.hotels,
        });
      })
      .catch((err) =>
        dispatch({
          type: GET_ALL_HOTELS_FAILURE,
          payload: err,
        }),
      );
  };
};
