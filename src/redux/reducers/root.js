import { combineReducers } from 'redux';
import hotel from './hotel';

export const rootReducer = combineReducers({
  hotel,
});
