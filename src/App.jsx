import React from 'react';
import './assets/scss/styles.scss';
import { Routers } from './routers/Router';

const App = () => {
  return (
    <div>
      <Routers />
    </div>
  );
};

export default App;
