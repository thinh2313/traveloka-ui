import React from 'react';

export const LayoutFull = ({ Header, Footer, children }) => {
  return (
    <div className="layout-full">
      <Header />
      {children}
      <Footer />
    </div>
  );
};
